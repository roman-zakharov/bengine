<script type="text/javascript">
$(document).ready(function(){
	$(".cache").css("float","none");
	$(".cache").click(function(){
		var rel = $(this).attr("rel");
		$.post("/admin/plugins/cache/"+rel+"/",function(data) {
			$(".cache[rel="+rel+"]").attr("src","/system/template/img/menu_"+data+".png");
		});
		return false;
	});
});
</script>

<fieldset>
	<legend>Управление плагинами:</legend>
	<table cellpadding="0" cellspacing="0" class="editor">
		<tr class="header">
			<td>Название</td>
			<td class="center" width="200px">Engname</td>
			<td class="center" width="120px">Управление</td>
		</tr>
		<?php
		if(count($content) > 0)
		{
			foreach($content as $v)
			{
				echo '
				<tr>
					<td>';
						if($v["install"] == 1) {
							echo '<a href="/admin/'.$v["name"].'/" title="">'.$v["title"].'</a>';
						} else {
							echo '<span style="margin-left: 6px;">'.$v["title"].'</span>';
						}
				echo '
					</td><td class="center">'.$v["name"].'</td>
					<td class="control">';
						if($v["install"] == 0) {
							echo '<a href="/admin/plugins/install/'.$v["id"].'/" title="Установить плагин в системе"><img src="/system/template/img/add.png" /></a>';
						} else {
							echo '<a href="/admin/plugins/uninstall/'.$v["id"].'/"  onclick="return confirm(\'Вы уверенны, что хотите УДАЛИТЬ плагин: '.$v["title"].'?\'); return false;" title="Удалить плагин из системы"><img src="/system/template/img/delete.png" /></a>';
							echo '<a href="/admin/plugins/database/'.$v["name"].'/" title="Проверить целостность конфигурации плагина"><img src="/system/template/img/database.png" /></a>';
							#echo '<a href="/admin/plugins/system/'.$v["name"].'/" title="Редактировать конфигурацию плагина"><img src="/system/template/img/reply.png" /></a>';
						}
				echo '
					</td>
				</tr>
				';
			}
		}
		?>
	</table>
</fieldset>