<form action="#" method="post">
	<fieldset>
		<legend>Список полей для пользователей:</legend>
		
		<table cellpadding="0" cellspacing="0" class="editor">
			<tr class="header">
				<td>Название поля</td>
				<td class="center" width="170px">Название поля</td>
				<td class="center" width="100px">Тип поля</td>
				<td class="center" width="120px">Управление</td>
			</tr>
			<?php
				if(isset($content) and count($content) > 0)
				{
					foreach($content as $v)
					{
						echo '<tr>
							<td>
								<a href="/admin/users/fields/up/'.$v["id"].'/"><img src="/system/template/img/up.png" /></a>
								<a href="/admin/users/fields/down/'.$v["id"].'/"><img src="/system/template/img/down.png" /></a>
								<a href="/admin/users/fields/edit/'.$v["id"].'/">'.$v["title"].'</a>
							</td>
							<td class="center">'.$v["name"].'</td>
							<td class="center">'.$v["type"].'</td>
							<td class="control">
								<a href="/admin/users/fields/delete/'.$v["id"].'/" onclick="return confirm(\'Вы уверенны, что хотите удалить поле: '.$v["title"].'?\'); return false;"><img src="/system/template/img/delete.png" /></a>
								<a href="/admin/users/fields/edit/'.$v["id"].'/"><img src="/system/template/img/reply.png" /></a>
							</td>
						</tr>';
					}
				}
			?>
		</table>
		
	</fieldset>
	<a href="/admin/users/fields/add/" class="button">Создать поле</a>
	<a href="/admin/users/" class="button">Управление пользователями</a>
</form>