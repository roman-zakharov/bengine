<form action="#" method="post">
	<fieldset>
		<legend>Список групп пользователей:</legend>
		
		<table cellpadding="0" cellspacing="0" class="editor">
			<tr class="header">
				<td>Название группы</td>
				<td class="center" width="120">Пользователей</td>
				<td class="center" width="200">Доступ к администрированию</td>
				<td class="center" width="120">Управление</td>
			</tr>
			<?php
				
				if(isset($groups) and count($groups) > 0)
				{
					foreach($groups as $v)
					{
						echo '<tr><td><a href="/admin/users/groups/edit/'.$v["id"].'/">'.$v["title"].'</a></td>';
						
						echo '<td class="center"><a href="/admin/users/filter/group/'.$v["id"].'/">'.$v["users"].'</a></td>';
						
						if($v["admin"] == 1) {
							echo '<td class="center">Разрешен</td>';
						} else {
							echo '<td class="center">Запрещен</td>';
						}
						
						echo '<td class="control">
								<a href="/admin/users/groups/delete/'.$v["id"].'/"><img src="/system/template/img/delete.png" /></a>
								<a href="/admin/users/groups/edit/'.$v["id"].'/"><img src="/system/template/img/reply.png" /></a>
							</td>
						</tr>';
					}
				}
			?>
		</table>
	</fieldset>
	<a href="/admin/users/groups/add/" class="button">Создать группу</a>
	<a href="/admin/users/" class="button">Вернуться</a>
</form>