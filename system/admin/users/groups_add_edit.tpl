<form id="edit" method="post">
	<fieldset>
	
		<?php
			if($nodes[3] == "add") {
				echo '<legend>Создание группы:</legend>';
			}
			elseif($nodes[3] == "edit") {
				echo '<legend>Редактирование группы:</legend>';
			}
		?>
		
		<label>Название группы:</label>
		<input type="text" name="title" value="<?php if(isset($group['title'])) { echo $group['title']; } ?>" maxlength="200" />
		<br />
		
		<label>Администрирование:</label>
		<select name="admin">
			<option value="0" <?php if(isset($group['title']) and $group['admin'] == 0) { echo 'selected="selected"'; } ?> >Запрещено</option>
			<option value="1" <?php if(isset($group['title']) and $group['admin'] == 1) { echo 'selected="selected"'; } ?> >Разрешено</option>
		</select>
		<br />
		
		<label>Доступ к разделам:</label>
		<table>
			<tr>
				<td><input type="checkbox" name="permission[pages]" class="checkbox" <?php if(isset($group["permission"]["pages"]) and $group["permission"]["pages"] == "on") { echo 'checked="checked"';} ?>> <span class="checkbox">Структура сайта</span></td>
				<td><input type="checkbox" name="permission[users]" class="checkbox" <?php if(isset($group["permission"]["users"]) and $group["permission"]["users"] == "on") { echo 'checked="checked"';} ?>> <span class="checkbox">Пользователи</span></td>
			</tr>
			<tr>
				<td><input type="checkbox" name="permission[plugins]" class="checkbox" <?php if(isset($group["permission"]["plugins"]) and $group["permission"]["plugins"] == "on") { echo 'checked="checked"';} ?>> <span class="checkbox">Плагины</span></td>
				<td><input type="checkbox" name="permission[config]" class="checkbox" <?php if(isset($group["permission"]["config"]) and $group["permission"]["config"] == "on") { echo 'checked="checked"';} ?>> <span class="checkbox">Конфигурация</span></td>
				<td></td>
			</tr>
		<?php
			if(isset($plugins) and count($plugins) > 0) {
				$i = 0;
				foreach($plugins as $k => $v) {
					if($i == 0) {
						echo '<tr>';
					}
					
					$i++;
					
					if(isset($group["permission"][$k]) and $group["permission"][$k] == "on") {
						echo '<td><input type="checkbox" name="permission['.$k.']" class="checkbox" checked="checked"> <span class="checkbox"><a href="/admin/'.$k.'/">'.$v.'</a></span></td>';
					} else {
						echo '<td><input type="checkbox" name="permission['.$k.']" class="checkbox"> <span class="checkbox"><a href="/admin/'.$k.'/">'.$v.'</a></span></td>';
					}
					
					if($i == 3) {
						echo '</tr>';
						$i = 0;
					}
				}
				if($i < 3) {
					echo '</tr>';
				}
			}
		?>
		</table>
		
	</fieldset>
	
	<?php
		if($nodes[3] == "add") {
			echo '<input type="submit" name="submit" value="Создать новую группу">';
		}
		elseif($nodes[3] == "edit") {
			echo '<input type="submit" name="submit" value="Редактировать группу">';
		}
	?>
	
	<a href="/admin/users/groups/" class="button">Управление группами</a>
	<a href="/admin/users/" class="button">Управление пользователями</a>
</form>