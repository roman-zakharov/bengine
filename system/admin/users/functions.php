<?php
if( !defined("BENGINE") ) { die ("Hacking!"); }

#Включение в меню
if(isset($nodes[2]) and $nodes[2] == "menu" and isset($nodes[3]) and (int)$nodes[3] > 0)
{
	$menu = onoff("users", $nodes[3], $nodes[2]);
	cacheDel("users");
	die("".$menu."");
	die("".$menu."");
}

################################################ РАБОТА С ПОЛЬЗОВАТЕЛЯМИ ################################################

#Добавление нового пользователя
if(isset($nodes[2]) and $nodes[2] == "add" and !isset($nodes[3]) and isset($_POST["submit"]) and count($_POST) > 1)
{
	$_POST["reg"] = DATETIME;
	$_POST["auht"] = DATETIME;
	$_POST["ip"] = IP;
	$_POST["root"] = 0;

	if($_POST["group"] == 0) {
		$_POST["group"] = 1;
	}

	if($_POST["passw"] != "") {
		$_POST["passw"] = md5($_POST["passw"]);
	} else {
		die("Заполните пароль пользователя");
	}

	if(add("users", $_POST)) {
		cacheDel("users");
		header("Location: /admin/users/");
		die();
	 }
	error('При добавлении пользователя возникла ошибка',__FILE__,__LINE__);
	die("При добавлении пользователя возникла ошибка");
}

################################################ РАБОТА С ПОЛЯМИ ################################################

#Добавление дополнительного поля
elseif(isset($nodes[2]) and $nodes[2] == "fields" and isset($nodes[3]) and $nodes[3] == "add")
{
	if(isset($_POST["submit"]))
	{
		#Проверяем, чтобы поле для таблицы было правильно названо
		if(isset($_POST["name"]) and $_POST["name"] == "") {
			$_POST["name"] = translit($_POST["title"]);
		} else {
			$_POST["name"] = translit($_POST["name"]);
		}

		#Определяем тип поля
		if($_POST["type"] == "input" or $_POST["type"] == "image" or $_POST["type"] == "file") {
			$sql_alter = "ALTER TABLE `users` ADD `".$_POST["name"]."` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL";
		} else {
			$sql_alter = "ALTER TABLE `users` ADD `".$_POST["name"]."` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL";
		}
		
		#Добавляем сначала поле
		if(doquery($sql_alter))
		{
			#Добавляем информацию о поле
			if(add("users_fields", $_POST) != true) {
				error('Ошибка добавления нового элемена в таблицу users_fields',__FILE__,__LINE__);
				die("Ошибка добавления нового элемена в таблицу users_fields");
			} else {
				cacheDel("users");
				header("Location: /admin/users/fields/");
				die();
			}
		}
		error('Ошибка добавления нового поля в таблицу users',__FILE__,__LINE__);
		die("Ошибка добавления нового поля в таблицу users");
	}
}

#Редактор дополнительного поля
elseif(isset($nodes[2]) and $nodes[2] == "fields" and isset($nodes[3]) and $nodes[3] == "edit" and isset($nodes[4]) and (int)$nodes[4] > 0 and isset($_POST["submit"]))
{
	#Проверяем, чтобы поле для таблицы было правильно названо
	if(isset($_POST["name"]) and $_POST["name"] == "") {
		$_POST["name"] = translit($_POST["title"]);
	} else {
		$_POST["name"] = translit($_POST["name"]);
	}
	
	#Определяем тип поля
	if($_POST["type"] == "input" or $_POST["type"] == "image" or $_POST["type"] == "file") {
		$sql_alter = "ALTER TABLE `users` CHANGE `".$_POST["oldname"]."` `".$_POST["name"]."` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL";
	} else {
		$sql_alter = "ALTER TABLE `users` CHANGE `".$_POST["oldname"]."` `".$_POST["name"]."` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL";
	}
	
	#Добавляем сначала поле
	if(doquery($sql_alter))
	{
		#Добавляем информацию о поле
		if(edit("users_fields", $_POST, $nodes[4]) != true) {
			die("Ошибка изменения элемена в таблице users_fields");
		} else {
			cacheDel("users");
			header("Location: /admin/users/fields/edit/".$nodes[4]."/");
			die();
		}
	}
	error('Ошибка изменения поля в таблице users',__FILE__,__LINE__);
	die("Ошибка изменения поля в таблице users");
}

#Удаление дополнительного поля
elseif(isset($nodes[2]) and $nodes[2] == "fields" and isset($nodes[3]) and $nodes[3] == "delete" and isset($nodes[4]) and (int)$nodes[4] > 0)
{
	$sql_query = doquery("SELECT * FROM `users_fields` WHERE id='".$nodes[4]."' LIMIT 1");
	if(dorows($sql_query) == 1)
	{
		$sql = doassoc($sql_query);
		
		if(doquery("ALTER TABLE `users` DROP `".$sql["name"]."`")) {
			doquery("UPDATE `users_fields` SET `order`=`order`-1 WHERE `order`>'".$sql["order"]."'");
			doquery("DELETE FROM `users_fields` WHERE id='".$nodes[4]."' LIMIT 1");
			cacheDel("users");
			header("Location: /admin/users/fields/");
			die();
		} else {
			die("Не удалось удалить поле ".$sql["name"]);
		}
	} else {
		die("Такой элемент не найден");
	}
}

#Редактирование поля
if(isset($nodes[2]) and $nodes[2] == "edit" and isset($nodes[3]) and (int)$nodes[3] > 0 and isset($_POST["submit"]) and count($_POST) > 1)
{
	if($_POST["passw"] == "") {
		unset($_POST["passw"]);
	} else {
		$_POST["passw"] = md5($_POST["passw"]);
	}
	
	$sql_query = doquery("SELECT * FROM `users` WHERE id='".$nodes[3]."' LIMIT 1");
	if(dorows($sql_query) == 1)
	{
		$sql = doassoc($sql_query);
		
		if($sql["root"] == 1 and $_SESSION["id"] != $sql["id"] and $_SESSION["root"] != 1) {
			die("Вы не можете редактировать этого пользователя!");
		}
		
		if(edit("users", $_POST, $nodes[3]) != true) {
			error('При редактировании пользователя возникли ошибки',__FILE__,__LINE__);
			die("При редактировании пользователя возникли ошибки");
		} else {
			cacheDel("users");
		}
	}
}

#Удаление пользователя
elseif(isset($nodes[2]) and $nodes[2] == "delete" and isset($nodes[3]) and (int)$nodes[3] > 0)
{
	$sql_query = doquery("SELECT * FROM `users` WHERE id='".$nodes[3]."' LIMIT 1");
	if(dorows($sql_query) == 1)
	{
		$sql = doassoc($sql_query);
		
		if($sql["root"] == 1) {
			die("Этого пользователя удалять запрещено");
		}
		else
		{		
			if(doquery("DELETE FROM `users` WHERE id='".$nodes[3]."' LIMIT 1")) {
				cacheDel("users");
				header("Location: /admin/users/");
				die();
			} else {
				die("Не удалось удалить пользователя");
			}
		}
	} else {
		die("Такой пользователь не найден");
	}
}

################################################ РАБОТА С ГРУППАМИ ################################################

#Добавление новой группы
if(isset($nodes[2]) and $nodes[2] == "groups" and isset($nodes[3]) and $nodes[3] == "add" and !isset($nodes[4]) and isset($_POST["submit"]) and count($_POST) > 1)
{
	$_POST["permission"] = serialize($_POST["permission"]);
	if(add("groups", $_POST)) {
		header("Location: /admin/users/groups/");
		die();
	}
	error('При добавлении группы возникла ошибка',__FILE__,__LINE__);
	die("При добавлении группы возникла ошибка");
}

#Редактирование группы
if(isset($nodes[2]) and $nodes[2] == "groups" and isset($nodes[3]) and $nodes[3] == "edit" and isset($nodes[4]) and (int)$nodes[4] > 0 and isset($_POST["submit"]) and count($_POST) > 1)
{
	$_POST["permission"] = serialize($_POST["permission"]);
	if(edit("groups", $_POST, $nodes[4]) != true) {
		die("При редактировании группы возникли ошибки");
	}
}

#Удаление группы
elseif(isset($nodes[2]) and $nodes[2] == "groups" and isset($nodes[3]) and $nodes[3] == "delete" and isset($nodes[4]) and (int)$nodes[4] > 0)
{
	$sql_query = doquery("SELECT * FROM `groups` WHERE id='".$nodes[4]."' LIMIT 1");
	if(dorows($sql_query) == 1)
	{
		$sql = doassoc($sql_query);
		
		if($sql["id"] != 1 and $sql["id"] != 2) {
			if(doquery("DELETE FROM `groups` WHERE id='".$nodes[4]."' LIMIT 1")) {
				header("Location: /admin/users/groups/");
				die();
			} else {
				die("Не удалось удалить группу ".$sql["title"]);
			}
		} else {
			die("Нельзя удалить группу ".$sql["title"]);
		}
	} else {
		die("Такая группа не найдена");
	}
}
?>