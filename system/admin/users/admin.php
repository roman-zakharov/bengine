<?php
if( !defined("BENGINE") ) { die ("Hacking!"); }

include_once(ROOT_DIR."/system/admin/users/functions.php");

#Список групп
$query_groups = doquery("SELECT * FROM `groups` ORDER BY id");
if(dorows($query_groups) > 0) {
	$rowsgroups = doarray($query_groups);
	foreach($rowsgroups as $v) {
		$groups[$v["id"]] = $v;
	}
}
	
################################################ РАБОТА С ПОЛЬЗОВАТЕЛЯМИ ################################################

#Добавление пользователя
if(isset($nodes[2]) and $nodes[2] == "add")
{
	#Список полей
	$query_users_fields = doquery("SELECT * FROM `users_fields` ORDER BY `order`");
	if(dorows($query_users_fields) > 0) {
		$fields = doarray($query_users_fields);
	}
	$body = "/system/admin/users/users_add_edit.tpl";
}

#Редактирование пользователя
elseif(isset($nodes[2]) and $nodes[2] == "edit" and isset($nodes[3]) and (int)$nodes[3] > 0)
{
	#Информация о пользователе
	$query_users = doquery("SELECT * FROM `users` WHERE id=".$nodes[3]." LIMIT 1");
	if(dorows($query_users) > 0) {
		$content = doassoc($query_users);
	}
	#Список полей
	$query_users_fields = doquery("SELECT * FROM `users_fields` ORDER BY `order`");
	if(dorows($query_users_fields) > 0) {
		$fields = doarray($query_users_fields);
	}
	
	$body = "/system/admin/users/users_add_edit.tpl";
}

################################################ РАБОТА С ГРУППАМИ ################################################

#Список групп
elseif(isset($nodes[2]) and $nodes[2] == "groups" and !isset($nodes[3]))
{
	foreach($groups as $k => $v) {
		$groups[$k]["users"] = docount("users","`group`='".$v["id"]."'");
	}
	$body = "/system/admin/users/groups.tpl";
}

#Создание группы
elseif(isset($nodes[2]) and $nodes[2] == "groups" and isset($nodes[3]) and $nodes[3] == "add")
{
	#Список плагинов
	$plugins_sql = doquery("SELECT name,title,admin FROM `plugins` WHERE `install`=1 ORDER BY id");
	if(dorows($plugins_sql) > 0) {
		$plugins = dokeyval($plugins_sql);
	}
	$body = "/system/admin/users/groups_add_edit.tpl";
}

################################################ РАБОТА С ПОЛЯМИ ################################################

#Список созданных полей для пользователей
elseif(isset($nodes[2]) and $nodes[2] == "fields" and !isset($nodes[3]))
{
	#Список полей
	$query_fields = doquery("SELECT * FROM `users_fields` ORDER BY `order`");
	if(dorows($query_fields) > 0) {
		$content = doarray($query_fields);
	}
	$body = "/system/admin/users/fields.tpl";
}

#Создание нового поля
elseif(isset($nodes[2]) and $nodes[2] == "fields" and isset($nodes[3]) and $nodes[3] == "add")
{
	$body = "/system/admin/users/fields_add_edit.tpl";
}

#Редактирование поля
elseif(isset($nodes[2]) and $nodes[2] == "fields" and isset($nodes[3]) and $nodes[3] == "edit" and isset($nodes[4]) and (int)$nodes[4] > 0)
{
	#Данные о редактируемом поле
	$query_fields = doquery("SELECT * FROM `users_fields` WHERE id=".$nodes[4]." LIMIT 1");
	if(dorows($query_fields) > 0) {
		$content = doassoc($query_fields);
	}
	$body = "/system/admin/users/fields_add_edit.tpl";
}

#Редактирование группы
elseif(isset($nodes[2]) and $nodes[2] == "groups" and isset($nodes[3]) and $nodes[3] == "edit" and isset($nodes[4]) and (int)$nodes[4] > 0)
{
	#Список плагинов
	$plugins_sql = doquery("SELECT name,title FROM `plugins` WHERE `install`=1 ORDER BY id");
	if(dorows($plugins_sql) > 0) {
		$plugins = dokeyval($plugins_sql);
	}
	#группа
	$query_group = doquery("SELECT * FROM `groups` WHERE id=".$nodes[4]." LIMIT 1");
	if(dorows($query_group) > 0) {
		$group = doassoc($query_group);
		#права доступа
		if($group["permission"] != "") {
			$group["permission"] = decode($group["permission"]);
			$group["permission"] = unserialize($group["permission"]);
		}
	}
	$body = "/system/admin/users/groups_add_edit.tpl";
}

################################################ ФИЛЬТР И ВСЕ ПОЛЬЗОВАТЕЛИ ################################################

#Редактирование пользователя
elseif(isset($nodes[2]) and $nodes[2] == "filter" and isset($nodes[3]) and $nodes[3] != "" and isset($nodes[4]) and $nodes[4] != "")
{
	#Навигация
	if(isset($_GET["p"]) and (int)$_GET["p"] > 0) {
		$nav = donav(20, "users", "`".$nodes[3]."`='".$nodes[4]."'", $_GET["p"]);
	} else {
		$nav = donav(20, "users", "`".$nodes[3]."`='".$nodes[4]."'");
	}
	$limit = "LIMIT ".$nav["start"].", ".$nav["num"]."";
	#Заменяем пробел
	$nodes[4] = str_replace("%20"," ",$nodes[4]);
	#Список пользователей
	$query_users = doquery("SELECT id,`root`,`login`,`menu`,`group`,`reg`,`auht`,DATE_FORMAT(`reg`,'%d.%m.%Y') AS `nreg`,DATE_FORMAT(`auht`,'%d.%m.%Y') AS `nauht`,`ip` FROM `users` WHERE `".$nodes[3]."`='".$nodes[4]."' ORDER BY id ".$limit);
	if(dorows($query_users) > 0) {
		$content = doarray($query_users);
	}

	$body = "/system/admin/users/users.tpl";
}

else
{
	if(isset($_POST["user_search"]) and isset($_POST["user_login"]) and $_POST["user_login"] != "") {
		#Список пользователей
		$query_users = doquery("SELECT id,`root`,`login`,`menu`,`group`,`reg`,`auht`,DATE_FORMAT(`reg`,'%d.%m.%Y') AS `nreg`,DATE_FORMAT(`auht`,'%d.%m.%Y') AS `nauht`,`ip` FROM `users` WHERE login LIKE '".$_POST["user_login"]."%' ORDER BY id");
		if(dorows($query_users) > 0) {
			$content = doarray($query_users);
		}
	}
	else
	{
		#Навигация
		if(isset($_GET["p"]) and (int)$_GET["p"] > 0) {
			$nav = donav(20, "users", "", $_GET["p"]);
		} else {
			$nav = donav(20, "users", "");
		}
		$limit = "LIMIT ".$nav["start"].", ".$nav["num"]."";
		
		#Список пользователей
		$query_users = doquery("SELECT id,`root`,`login`,`menu`,`group`,`reg`,`auht`,DATE_FORMAT(`reg`,'%d.%m.%Y') AS `nreg`,DATE_FORMAT(`auht`,'%d.%m.%Y') AS `nauht`,`ip` FROM `users` ORDER BY id ".$limit);
		if(dorows($query_users) > 0) {
			$content = doarray($query_users);
		}
	}
	$body = "/system/admin/users/users.tpl";
}
?>