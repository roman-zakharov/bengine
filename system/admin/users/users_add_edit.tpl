<?php include_once(ROOT_DIR."/system/template/tinymce_br.tpl"); ?>

<form id="edit" method="post">
	<fieldset>
	
		<legend>Редактор пользователя:</legend>
		<label>Логин пользователя:</label> <input type="text" name="login" value="<?php if(isset($content["login"])) {echo $content["login"];} ?>" /><br />
		<label>Email пользователя:</label> <input type="text" name="email" value="<?php if(isset($content["email"])) {echo $content["email"];} ?>" /><br />
		<label>Новый пароль:</label> <input type="text" name="passw" value=""><br />
		<label>Группа:</label>
		<select name="group">
				<option value="0">...</option>
				<?php
					foreach($groups as $v)
					{
						if(isset($content["group"])) {
							if($content["group"] == $v["id"]) {
								echo '<option value="'.$v["id"].'" selected="selected">'.$v["title"].'</option>';
							} else {
								echo '<option value="'.$v["id"].'">'.$v["title"].'</option>';
							}
						} else {
							echo '<option value="'.$v["id"].'">'.$v["title"].'</option>';
						}
					}
				?>
		</select>
		<br />	
		<?php
			if(isset($fields) and count($fields) > 0) {
				foreach($fields as $v) {
					echo '<label>'.$v["title"].':</label>';
					if(isset($content[$v["name"]]) and $content[$v["name"]] != "") {
						$value_row_user = $content[$v["name"]];
					} else {
						$value_row_user = "";
					}
					if($v["type"] == "textarea") {
						echo '<textarea name="'.$v["name"].'" style="width: 70%; height: 100px;">'.$value_row_user.'</textarea><br />';
					} else {
						echo '<input type="text" name="'.$v["name"].'" id="'.$v["name"].'" value="'.$value_row_user.'" />';
						if($v["type"] == "image" or $v["type"] == "file") {
							echo '<a href="#" onclick="elFinderBrowser(\''.$v["name"].'\', \''.$value_row_user.'\', \'images\', window);"><img src="/system/template/img/view.png" class="addImg"></a>';
						}
						echo '<br />';
					}
				}
			}
		?>
		
		<textarea name="tinymce" style="display: none; width: 0px; height: 0px;"></textarea>
		
	</fieldset>
	
	<?php
	if(isset($nodes[2]) and $nodes[2] == "add") {
		echo '<input type="submit" name="submit" class="button" value="Создать пользователя">';
	}
	elseif(isset($nodes[2]) and $nodes[2] == "edit") {
		echo '<input type="submit" name="submit" class="button" value="Редактировать пользователя">';
	}
	?>
	<a href="/admin/users/" class="button">Управление пользователями</a>
	
</form>