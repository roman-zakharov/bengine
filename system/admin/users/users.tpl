<script type="text/javascript">
$(document).ready(function(){
	$(".click_menu").click(function(){
		var rel = $(this).attr("rel");
		$.post("/admin/users/menu/"+rel+"/",function(data) {
			$(".click_menu[rel="+rel+"]").attr("src","/system/template/img/menu_"+data+".png");
		});
		return false;
	});
});
</script>

<form action="#" method="post">
	<fieldset>
		<legend>Пользователи сайта:</legend>
		
		<table cellpadding="0" cellspacing="0" class="editor tablesorter" id="usersTable">
			<thead>
			<tr class="header">
				<td>Логин</td>
				<td class="center" width="150px">Группа</td>
				<td class="center" width="100px">Авторизация</td>
				<td class="center" width="110px">IP</td>
				<td class="center" width="100px">Регистрация</td>
				<td class="center" width="120px">Управление</td>
			</tr>
			</thead>
			<tbody>
			<?php
				if(isset($content) and count($content) > 0)
				{
					foreach($content as $v)
					{
						if(isset($groups[$v["group"]]["id"])) {
							$group_id = $groups[$v["group"]]["id"]."/";
							$group_title = $groups[$v["group"]]["title"];
						} else {
							$group_id = "";
							$group_title = "Без группы";
						}
						
						echo '<tr>
							<td>
								<img src="/system/template/img/menu_'.$v["menu"].'.png" class="click_menu" rel="'.$v["id"].'" />
								<a href="/admin/users/edit/'.$v["id"].'/" title="Редактировать пользователя">'.$v["login"].'</a>
							</td>
							<td class="center"><a href="/admin/users/filter/group/'.$group_id.'">'.$group_title.'</a></td>
							<td class="center"><span title="'.$v["auht"].'">'.$v["nauht"].'</span></td>
							<td class="center"><a href="/admin/users/filter/ip/'.$v["ip"].'/">'.$v["ip"].'</a></td>
							<td class="center"><span title="'.$v["reg"].'">'.$v["nreg"].'</span></td>
							<td class="control">';
						
						if($v["root"] == 1) {
							echo '<a title="Удалить пользователя"><img src="/system/template/img/delete.png"></a> ';
						} else {
							echo '<a href="/admin/users/delete/'.$v["id"].'/" onclick="return confirm(\'Вы уверенны, что хотите удалить пользователя: '.$v["login"].'?\'); return false;" title="Удалить пользователя"><img src="/system/template/img/delete.png"></a> ';
						}
						if($_SESSION["root"] == 0 and $v["root"] == 1) {
							echo '';
						} else {
							echo '<a href="/admin/users/edit/'.$v["id"].'/" title="Редактировать пользователя"><img src="/system/template/img/reply.png"></a> ';
						}
						echo '</td></tr>';
					}
				}
			?>
			</tbody>
		</table>
		
		<?php	
		if(isset($nav["list"]) and count($nav["list"]) > 1)
		{
			$countlist =  count($nav["list"]); #10
			$paginator = paginator(10);
			echo '<br /><a href="?p=1" class="nav">«</a>';
			foreach($paginator as $v)
			{
				if($p == $v) {
					echo '<span class="nav">'.$v.'</span>';
				} else {
					echo '<a href="?p='.$v.'" class="nav">'.$v.'</a>';
				}
			}
			echo '<a href="?p='.$countlist.'" class="nav">»</a>';
			
		}
		?>
	</fieldset>
	
		<a href="/admin/users/add/" class="button">Создать пользователя</a>
		<a href="/admin/users/fields/" class="button">Управление полями</a>
		<a href="/admin/users/groups/" class="button">Управление группами</a>
		<input type="text" name="user_login" value="" style="width: 150px; margin-right: 5px;">
		<input type="submit" name="user_search" class="button" value="Найти пользователя">
</form>