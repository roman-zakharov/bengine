<form id="edit" method="post">

	<fieldset>
		<legend>Редактор поля:</legend>
		<label>Заголовок поля:</label><input type="text" name="title" value="<?php if(isset($content["title"])) {echo $content["title"];} ?>" maxlength="50" /><br />
		<label>Транслитом:</label><input type="text" name="name" value="<?php if(isset($content["name"])) {echo $content["name"];} ?>" maxlength="10" /><br />
		<label>Тип поля:</label>
		<select name="type">
			<option value="input" <?php if(isset($content["type"]) and $content["type"] == "input") {echo 'selected="selected"';} ?> >INPUT (одна строка)</option>
			<option value="textarea" <?php if(isset($content["type"]) and $content["type"] == "textarea") {echo 'selected="selected"';} ?> >TEXTAREA (несколько строк)</option>
			<option value="image" <?php if(isset($content["type"]) and $content["type"] == "image") {echo 'selected="selected"';} ?> >IMAGE (изображение)</option>
			<option value="file" <?php if(isset($content["type"]) and $content["type"] == "file") {echo 'selected="selected"';} ?> >FILE (файл или ссылка)</option>
		</select><br />
		<label>Минимум символов: <br />
		<span>
		0 - без ограничений
		</span>
		</label>
		<input type="text" name="minimal" value="<?php if(isset($content["minimal"])) {echo $content["minimal"];} else {echo "0";} ?>" maxlength="3" /><br />
		<label>Обязательно:</label>
		<select name="check">
			<option value="0" <?php if(isset($content["check"]) and $content["check"] == 0) {echo 'selected="selected"';} ?>>Можно не заполнять</option>
			<option value="1" <?php if(isset($content["check"]) and $content["check"] == 1) {echo 'selected="selected"';} ?>>Обязательно</option>
		</select><br />
	</fieldset>
	
	<?php
	if(isset($nodes[3]) and $nodes[3] == "add") {
		echo '<input type="submit" name="submit" class="button" value="Создать поле">';
	}
	elseif(isset($nodes[3]) and $nodes[3] == "edit") {
		echo '<input type="submit" name="submit" class="button" value="Редактировать поле">';
	}
	?>
	<input type="hidden" name="oldname" value="<?php if(isset($content["name"])) {echo $content["name"];} ?>" />
	<a href="/admin/users/fields/" class="button">Управление полями</a>
	<a href="/admin/users/" class="button">Управление пользователями</a>
	
</form>