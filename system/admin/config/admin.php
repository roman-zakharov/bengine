<?php
if( !defined("BENGINE") ) { die ("Hacking!"); }

if(isset($_POST["edit_config"]))
{
	#Обработка пост-данных
	foreach($_POST as $k => $v)
	{
		$in = array("\"","`","'","\\","<",">","′","’");
		$ou = array("&quot;","&#096;","&#039;","\\\\","&lt;","&gt;","&prime;","&rsquo;");
		$_POST[$k] = str_replace($in,$ou,$v);
	}

	#Обработка некоторых элементов
	$_POST["url"] = str_replace("http://","",$_POST["url"]);
	$_POST["url"] = str_replace("/","",$_POST["url"]);
	
	#Изменяем конфигурацию
	foreach($_POST as $k => $v) {
		if(isset($cfg[$k]) and $cfg[$k] != $_POST[$k]) {
			$sql_config_edit[] = "UPDATE config SET value='".$v."' WHERE module='system' and type='".$k."' LIMIT 1";
		}
	}
	if(isset($sql_config_edit) and count($sql_config_edit) > 0) {
		foreach($sql_config_edit as $v) {
			if(!doquery($v)) {
				error('Ошибка сохранения конфигурации системы: '.$v,__FILE__,__LINE__);
				die("Ошибка сохранения конфигурации системы: ".$v);
			}
		}
	}
	header("Location: /admin/config/#");
	die();
}

#Список страниц для выставления по умолчанию
$query_pages = doquery("SELECT `id`,`title` FROM `pages` ORDER BY `id`,`title`");
if(dorows($query_pages) > 0) {
	$content["pages"] = doarray($query_pages);
}

#Список шаблонов сайта
$content["template"] = bengine_dirs(ROOT_DIR."/template");

#Список файлов в шаблоне сайта
$content["template_files"] = bengine_files(ROOT_DIR."/template");

$body = "/system/admin/config/config.tpl";
?>