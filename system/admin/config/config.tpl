<form action="/admin/config/" method="post">

	<fieldset>
		<legend>Конфигурация системы:</legend>
		
		<label>Адрес (URL) сайта:</label> <input type="text" name="url" maxlength="255" value="<?php echo $cfg["url"]; ?>" /> <br />
		<label>E-mail администратора:<br /><span>можно указать несколько адресов через запятую</span></label> <input type="text" name="email" maxlength="255" value="<?php echo $cfg["email"]; ?>" /><br />
		<label>Заголовок сайта:</label> <input type="text" name="title" maxlength="250" value="<?php echo $cfg["title"]; ?>" /> <br />
		
		<?php
		if(SEO == true) {
			echo '<label>Описание сайта:</label> <input type="text" name="description" maxlength="250" value="'.$cfg["description"].'" /><br />';
			echo '<label>Ключевые слова сайта:</label> <input type="text" name="keywords" maxlength="250" value="'.$cfg["keywords"].'" /><br />';
		}
		?>
		
		<label>Страница по умолчанию:</label>
		<select name="page">
			<option value="0">---</option>
			<?php
				if(isset($content["pages"]) and count($content["pages"]) > 0)
				{
					foreach($content["pages"] as $v)
					{
						if($cfg["page"] == $v["id"]) {
							echo '<option value="'.$v["id"].'" selected="selected">'.$v["title"].'</option>';
						} else {
							echo '<option value="'.$v["id"].'">'.$v["title"].'</option>';
						}
					}
				}
			?>
		</select><br />
		<label>Ошибки сайта:</label>
		<select name="errors">
			<option value="1" <?php if($cfg["errors"] == "1") {echo 'selected="selected"';} ?>>Показывать</option>
			<option value="0" <?php if($cfg["errors"] == "0") {echo 'selected="selected"';} ?>>Скрывать</option>
		</select><br />	
		
		<label>Файл ошибки 404:</label>
		<select name="error404">
		<?php
			if(isset($content["template_files"]) and count($content["template_files"]) > 0)
			{
				foreach($content["template_files"] as $f)
				{
					echo '<option value="'.$f.'"';
					if($cfg["error404"] == $f) {
						echo ' selected="selected"';
					}
					echo '>'.$f.'</option>';
				}
			}
		?>
		</select><br />
	</fieldset>
	
	<fieldset>
		<legend>Доступ к сайту:</legend>
		<label>Отключение сайта:</label>
		<select name="offline">
			<option value="1" <?php if($cfg["offline"] == 1) {echo 'selected="selected"';} ?>>Онлайн</option>
			<option value="0" <?php if($cfg["offline"] == 0) {echo 'selected="selected"';} ?>>Офлайн</option>
		</select><br />
		<label>Причина отключения:</label> <textarea name="offline_title" rows="4" cols="30"><?php echo $cfg["offline_title"]; ?></textarea> <br />
	</fieldset>
	
	<fieldset>
		<legend>Шаблоны страниц по умолчанию:</legend>
		
		<label>Верх страницы:</label>
		<select name="header">
		<?php
			if(isset($content["template_files"]) and count($content["template_files"]) > 0)
			{
				foreach($content["template_files"] as $f)
				{
					echo '<option value="'.$f.'"';
					if($cfg["header"] == $f) {
						echo ' selected="selected"';
					}
					echo '>'.$f.'</option>';
				}
			}
		?>
		</select><br />
		<label>Тело страницы:</label>
		<select name="body">
		<?php
			if(isset($content["template_files"]) and count($content["template_files"]) > 0)
			{
				foreach($content["template_files"] as $f)
				{
					echo '<option value="'.$f.'"';
					if($cfg["body"] == $f) {
						echo ' selected="selected"';
					}
					echo '>'.$f.'</option>';
				}
			}
		?>
		</select><br />
		<label>Низ страницы:</label>
		<select name="footer">
		<?php
			if(isset($content["template_files"]) and count($content["template_files"]) > 0)
			{
				foreach($content["template_files"] as $f)
				{
					echo '<option value="'.$f.'"';
					if($cfg["footer"] == $f) {
						echo ' selected="selected"';
					}
					echo '>'.$f.'</option>';
				}
			}
		?>
		</select><br />
	</fieldset>
	
	<input type="hidden" name="edit_config" />
	<input type="submit" name="submit" value="Сохранить конфигурацию">
</form>