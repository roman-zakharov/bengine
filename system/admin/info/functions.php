<?php
if( !defined("BENGINE") ) { die ("Hacking!"); }

if(isset($_GET["data"]))
{	
	#Версии Bengine для пользователя
	if($_GET["data"] == "version_update")
	{	
		if(UPDATE == false) {
			die();
		}

		if(isset($cfg["url"]) and $cfg["url"] != "") { $url = "&url=".$cfg["url"]; }else{ $url = ""; }		
		if(($new = @file_get_contents("http://upd.bengine.ru/index.php?version=".$cfg["vers"].$url)) != false)
		{		
			$new = strip_tags($new);
			$replace_new = preg_replace('/\D/', '$1', $new);
			$replace_vers = preg_replace('/\D/', '$1', $cfg["vers"]);			
			if($replace_new < 300) { $replace_new = $replace_new * 10; }
			if($replace_vers < 300) { $replace_vers = $replace_vers * 10; }			
			$replace_new > $replace_vers ? die("".$new."") : die();			
		}
	}
	#Очистка кэша системы
	elseif($_GET["data"] == "cache")
	{
		cacheDel();
		die("Очистка завершена");
	}
	elseif($_GET["data"] == "notepad")
	{
		if(isset($_POST["notepad"])) {
			$notepad = htmlspecialchars(trim($_POST["notepad"]), ENT_QUOTES, "UTF-8");
			$notepad = nl2br($notepad);
			if(doquery("UPDATE `config` SET value='".$notepad."' WHERE `module`='system' and `type`='notepad' LIMIT 1")) {
				die("Сохранить запись в блокноте");
			} else {
				error('Ошибка сохранения данных в блокноте',__FILE__,__LINE__);
				die("Ошибка сохранения данных");
			}
		}
	}
	else
	{
		die("Ошибка #4: неверный запрос");
	}
}

?>