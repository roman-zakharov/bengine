<?php
if( !defined("BENGINE") ) { die ("Hacking!"); }

include_once(ROOT_DIR."/system/admin/info/functions.php");

#Дополнительная информация
$cfg["offline"] == 1 ? $content["offline"] = "Включен" : $content["offline"] = "Отключен";
$cfg["errors"] == 1 ? $content["errors"] = "Включен" : $content["errors"] = "Отключен";

#Версия PHP
$phpversion = explode("-",phpversion());
$phpversion = $phpversion[0];

#Версия MySQL
$mysqlversion = doqueryassoc("SELECT VERSION()");
$mysqlversion = explode("-",$mysqlversion["VERSION()"]);
$mysqlversion = $mysqlversion[0];

#Версия Apache
$apacheversion = explode(" ",$_SERVER['SERVER_SOFTWARE']);
$apacheversion = str_replace("Apache/","",$apacheversion[0]);

#Количество страниц в системе
$pagescount = docount("pages");

#Количество установленных плагинов
$pluginscount = 0;

#Количество зарегистрированных пользователей
$userscount = docount("users");

#Установленных плагинов
$pluginscount = docount("plugins","`install`=1");

#блокнот
$cfg["notepad"] = str_replace("<br />","",$cfg["notepad"]);

#Работа с плагинами
$plugins_query = doquery("SELECT * FROM `plugins` WHERE `admin`!=1 and `install`=1 ORDER BY `title`");
if(dorows($plugins_query) > 0) {
	$plugins = doarray($plugins_query);
}

#Ошибки сайта
if(file_exists(ROOT_DIR."/install.php")) {
	$content["file_errors"][] = "Удалите файл install.php";
}
if(file_exists(ROOT_DIR."/update.php")) {
	$content["file_errors"][] = "Установите обновление перейдя по <a href='/update.php'>ссылке</a>, а затем удалите файл update.php";
}
if($cfg["errors"] == 1) {
	$content["file_errors"][] = "Отключите вывод ошибок системы";
}
if($cfg["offline"] == 0) {
	$content["file_errors"][] = "Сайт отключен, данные не видны посетителю";
}
if($pagescount == 0) {
	$content["file_errors"][] = "Нет ни одной созданной вами страницы";
}
if($cfg["page"] == 0) {
	$content["file_errors"][] = "Не установлена, либо отсутствует, страница по умолчанию";
}
if(str_replace("www.","",$cfg["url"]) != str_replace("www.","",$_SERVER["HTTP_HOST"])) {
	$content["file_errors"][] = "Проверьте адрес (URL) сайта в конфигурации";
}

$body = "/system/admin/info/info.tpl";
?>