<script type="text/javascript">
$(document).ready(function(){
	//Проверка новой версии
	$.get('/admin/info/?data=version_update', function(data) {
		if(data != '') {
			$('#update').html('<a href="http://bengine.ru/" target="_blank">Bengine ' + data +'</a>');
		}
	});
	//Очистка кэша
	$('#cache').click(function(){
		$('#cache').text("очистка кэша...");
		$.get("/admin/info/?data=cache", function(data){
			$('#cache').text(data);
		});
		return false;
	});
	//Сохранение блокнота
	$('#notepad').click(function(){
		$('#notepad').text("сохраняем запись в БД системы...");
		var notepad = $("#textnotepad").val();
		$.post("/admin/info/?data=notepad", {notepad:notepad}, function(data){
			$('#notepad').text(data);
		});
		return false;
	});
});
</script>

<?php
	if(isset($plugins) and count($plugins) > 0)
	{
		echo '<fieldset><legend>Быстрый доступ к плагинам:</legend>';	
		foreach($plugins as $v)
		{
			if(file_exists(ROOT_DIR."/plugins/".$v["name"]."/icon.png")) {
				$pluginIcon = "/plugins/".$v["name"]."/icon.png";
			} else {
				$pluginIcon = "/system/template/img/plugin.png";
			}
			echo '
				<a href="/admin/'.$v["name"].'/">
					<div class="plugins" title="'.$v["title"].'">
						<img src="'.$pluginIcon.'" style="width: 32px; height: 32px;"><div>'.$v["title"].'</div>
					</div>
				</a>
			';
		}
		echo '</fieldset>';
	}
	
	if(isset($content["file_errors"]) and count($content["file_errors"]) > 0)
	{
		echo "<fieldset style=\"border-color: red; background-color: #fdf8f8;\">";
		echo "<legend style=\"color:red;\">Внимание, на сайте возможны ошибки:</legend>";
		foreach($content["file_errors"] as $er)
		{
			echo "".$er."<br />";
		}
		echo "</fieldset>";
	}
?>

<style>
input {
	width: 120px !important;
}
label {
	width: 170px !important;
	min-width: 170px !important;
}
#textnotepad {
	width: 99%;
	background-color: #ffffe4;
	font-family: 'Comic Sans MS', Tahoma, sans-serif;
}
</style>

<fieldset>
	<legend>Статистика сайта:</legend>
	<div style="float: left;">
		<label>Версия Bengine:</label> <input class="info" value="<?php echo $cfg["vers"]; ?>" readonly="readonly" />
		<label>Размер кэша:</label> <input class="info" value="<?php echo round(bengine_dir_size(ROOT_DIR."/cache")/1024/1024,3); ?> Mb" readonly="readonly" />
		<label>Размер БД:</label> <input class="info" value="<?php echo round(dosize()/1024/1024,3); ?> Mb" readonly="readonly" />
	</div>
	<div style="float: left;">
		<label>Плагинов:</label> <input class="info" value="<?php echo $pluginscount; ?>" readonly="readonly" />
		<label>Количество страниц:</label> <input class="info" value="<?php echo $pagescount; ?>" readonly="readonly" />
		<label>Пользователей:</label> <input class="info" value="<?php echo $userscount; ?>" readonly="readonly" />
	</div>
</fieldset>

<fieldset>
	<legend>Блокнот для заметок:</legend>
	<textarea id="textnotepad" rows="6"><?php echo $cfg["notepad"]; ?></textarea>
</fieldset>

<a href="#" class="button" id="notepad">Сохранить запись в блокноте</a>
<a href="#" class="button" id="cache">Очистить кэш сайта</a><br />

<fieldset>
	<legend>Информация о системе:</legend>
	<div style="float: left;">
		<label>Версия Apache:</label> <input class="info" value="<?php echo $apacheversion; ?>" readonly="readonly" />
		<label>Версия PHP:</label> <input class="info" value="<?php echo $phpversion; ?>" readonly="readonly" />
		<label>Версия MySQL:</label> <input class="info" value="<?php echo $mysqlversion; ?>" readonly="readonly" />
		<label>Вывод ошибок:</label> <input class="info" value="<?php echo $content["errors"]; ?>" readonly="readonly" />
		<label>Работа сайта:</label> <input class="info" value="<?php echo $content["offline"]; ?>" readonly="readonly" />
	</div>
	<div style="float: left;">
		<label>IP адрес сайта:</label> <input class="info" value="<?php echo $_SERVER["SERVER_ADDR"]; ?>" readonly="readonly" />
		<label>Свободное место:</label> <input class="info" value="<?php echo round(disk_free_space(ROOT_DIR)/1024/1024/1024,2); ?> Gb" readonly="readonly" />
		<label>Оперативной памяти:</label> <input class="info" value="<?php echo ini_get("memory_limit"); ?>" readonly="readonly" />
		<label>MAX размер файла:</label> <input class="info" value="<?php echo ini_get("upload_max_filesize"); ?>" readonly="readonly" />
		<label>MAX время загрузки:</label> <input class="info" value="<?php echo ini_get("max_input_time"); ?> сек" readonly="readonly" />
	</div>
</fieldset>