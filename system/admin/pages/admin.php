<?php
if( !defined("BENGINE") ) { die ("Hacking!"); }

#Присвоение кэшу массива страниц сайта
$cache_pages = cacheGet("pages");

include_once(ROOT_DIR."/system/admin/pages/functions.php");

#Список возможных плагинов
$plugins_query = doquery("SELECT name,title,admin FROM `plugins` WHERE `install`=1 ORDER BY `title`");
if(dorows($plugins_query) > 0) {
	$plugins = doarray($plugins_query);
}
	
#Добавление страницы
if(isset($nodes[2]) and $nodes[2] == "add")
{
	#Список файлов в шаблоне сайта
	$content["template_files"] = bengine_files(ROOT_DIR."/template");
	
	$body = "/system/admin/pages/pages_add_edit.tpl";
}
#Редакирование страницы
elseif(isset($nodes[2]) and $nodes[2] == "edit" and isset($nodes[3]) and (int)$nodes[3] > 0)
{
	#Данные редактируемой страницы
	$pages_query = doquery("SELECT * FROM `pages` WHERE `id`=".$nodes[3]." LIMIT 1");
	if(dorows($pages_query) == 1) {
		$content = doassoc($pages_query);
	}
	
	#Список файлов в шаблоне сайта
	$content["template_files"] = bengine_files(ROOT_DIR."/template");
	
	$body = "/system/admin/pages/pages_add_edit.tpl";
}
#Список страниц
else
{
	$pages_query = doquery("SELECT `id`,`parent`,`child`,`showchild`,`menu`,`order`,`engname`,`plugin`,`title` FROM `pages` WHERE `parent`=0 ORDER BY `order`");
	if(dorows($pages_query) > 0) {
		$content = doarray($pages_query);
	}
	
	$plugin["pages"] = "";
	if(isset($plugins) and count($plugins) > 0) {
		foreach($plugins as $v) {
			$plugin[$v["name"]] = $v["title"];
		}
	}

	$body = "/system/admin/pages/pages.tpl";
}
?>