<?php include_once(ROOT_DIR."/system/template/tinymce.tpl"); ?>


<script type="text/javascript">
$(document).ready(function(){
	$('input[name=submit]').click(function(){
		var title = $('input[name=title]').val();
		if(title == '') {
			alert('Заполните Заголовок страницы');
			return false;
		}
	});
});
</script>

<?php
	$templates = bengine_files(ROOT_DIR."/template/");
        if(isset($content["image"]) and $content["image"] != "") {
            $finderImg = $content["image"];
        } else {
            $finderImg = "";
        }
?>

<form action="#" method="post">
	<fieldset>	
	<legend><?php if($nodes[2] == "add") {echo 'Создание';} else {echo 'Редактирование';} ?> страницы</legend>
		<label>Заголовок страницы:</label> <input type="text" name="title" value="<?php if(isset($content["title"])) {echo $content["title"];} ?>" maxlength="200" /><?php if(isset($content["title"])) { echo '<a href="/'.$content["engname"].'/" target="_blank"><img src="/system/template/img/eye.png" class="addImg" /></a>';} ?><br />
		<label>URL страницы:</label> <input type="text" name="engname" value="<?php if(isset($content["engname"])) {echo $content["engname"];} ?>" maxlength="50" /><br />
		<label class="seo">Описание страницы:</label> <input type="text" name="description" class="seo" value="<?php if(isset($content["description"])) {echo $content["description"];} ?>" maxlength="200" /><br class="seo" />
		<label class="seo">Ключевые слова:</label> <input type="text" name="keywords" class="seo" value="<?php if(isset($content["keywords"])) {echo $content["keywords"];} ?>" maxlength="200" /><br class="seo" />
		<label>Изображение:</label> <input type="text" name="image" id="image" value="<?php if(isset($content["image"])) {echo $content["image"];} ?>" maxlength="200" />
		<a href="#" onclick="elFinderBrowser('image', '<?php $finderImg ?>', 'images', window);"><img src="/system/template/img/view.png" class="addImg"></a>
		<br />
		
		<label>Наполнение страницы: <br /><a class="btn" href="javascript:;" onclick="tinymce.execCommand('mceToggleEditor',false,'text');"><span>Отключить TinyMCE</span></a></label>
		<div style="float: left; width: 80%;">
			<textarea name="text" style="width: 100%; height: 400px;"><?php if(isset($content["text"])){echo $content["text"];} ?></textarea>
		</div>
		<br />
		<br />
		
		<?php
			if(isset($plugins) and count($plugins) > 0) {
				echo '<label>Плагин:</label><select name="plugin"><option value="pages"></option>';
				foreach($plugins as $v) {
					if(isset($content["plugin"]) and $content["plugin"] == $v["name"]) {
						echo '<option value="'.$v["name"].'" selected="selected">'.$v["title"].'</option>';
					} else {
						echo '<option value="'.$v["name"].'">'.$v["title"].'</option>';
					}
				}
				echo '</select><br />';
			} else {
				echo '<input type="hidden" name="plugin" value="pages">';
			}
		?>

		<label>Верх страницы:</label>
		<?php echo selected($templates,"header"); ?><br />
		
		<label>Тело страницы:</label>
		<?php echo selected($templates,"body"); ?><br />
		
		<label>Низ страницы:</label>
		<?php echo selected($templates,"footer"); ?><br />		
	</fieldset>
	
	<input type="submit" name="submit" value="<?php if($nodes[2] == "add") {echo 'Создать';} else {echo 'Сохранить';} ?> страницу">
	<?php if(isset($content["engname"])) {echo '<a href="/'.$content["engname"].'/" target="_blank" class="button">Просмотр страницы</a>';} ?>
	<a href="/admin/pages/" class="button">Вернуться</a>
	
</form>