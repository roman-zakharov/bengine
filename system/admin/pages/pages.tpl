<?php
function structure($parent, $level = 1)
{
	global $plugin;
	$structure = array();
	if(($sql_structure = doquery("SELECT `id`,`parent`,`child`,`showchild`,`menu`,`order`,`engname`,`plugin`,`title` FROM `pages` WHERE `parent`=".$parent." ORDER BY `order`")) != false)
	{
		if(dorows($sql_structure) > 0) {
			$structure = doarray($sql_structure);
			foreach($structure as $k => $v)
			{
				if(mb_strlen($v["title"]) > 100) {
					$v["title"] = mb_substr($v["title"],0,100)."...";
				}
				$padding_level = (40*$level)+13;
				echo '
				<tr>
					<td style="padding-left: '.$padding_level.'px">
						<a href="/admin/pages/up/'.$v["id"].'/" title="Переместить вверх"><img src="/system/template/img/up.png" /></a>
						<a href="/admin/pages/dn/'.$v["id"].'/" title="Переместить вниз"><img src="/system/template/img/down.png" /></a>
						<a href="/admin/pages/add/'.$v["id"].'/" title="Добавить вложенную страницу"><img src="/system/template/img/addsmall.png" /></a>
						<img src="/system/template/img/menu_'.$v["menu"].'.png" class="click_menu" rel="'.$v["id"].'" title="Включить меню" />
					';
					
					if($v["child"] > 0) {
						if($v["showchild"] == 1) {
							echo '<a href="/admin/pages/showchild/'.$v["id"].'/0/" title="Скрыть внутренние страницы"><img src="/system/template/img/minimize.png" /></a>';
						} else {
							echo '<a href="/admin/pages/showchild/'.$v["id"].'/1/" title="Просмотр внутренних страниц"><img src="/system/template/img/maximize.png" /></a>';
						}
					}
					
					echo '
						<a href="/admin/pages/edit/'.$v["id"].'/">'.$v["title"].'</a>
					</td>
					<td class="center">'.$v["id"].'</td>
				';
					
				if($v["plugin"] == "pages") {
					echo '<td class="center"></td>';
				} else {
					echo '<td class="center"><a href="/admin/'.$v["plugin"].'/" title="Управление данными плагина">'.$v["plugin"].'</a></td>';
				}
					
				echo '
					<td class="control">
						<a href="/'.$v["engname"].'/" target="_blank"><img src="/system/template/img/eye.png" title="Просмотр страницы на сайте" /></a>';
						if($v["child"] > 0) {
							echo '<img src="/system/template/img/delete.png" style="opacity:0.5; cursor: default;" />';
						} else {
							echo '<a href="/admin/pages/delete/'.$v["id"].'/" onclick="return confirm(\'Вы уверенны, что хотите удалить страницу: '.$v["title"].'?\'); return false;" title="Удалить страницу"><img src="/system/template/img/delete.png" /></a>';
						}
					echo '<a href="/admin/pages/edit/'.$v["id"].'/"><img src="/system/template/img/reply.png" title="Редактировать страницу" /></a>
					</td>
				</tr>
				';
				if($v["child"] > 0 and $v["showchild"] == 1) {
					structure($v["id"],$level+1);
				}
			}
		}
	}
}
?>

<script type="text/javascript">
$(document).ready(function(){
	$(".click_menu").click(function(){
		var rel = $(this).attr("rel");
		$.post("/admin/pages/menu/"+rel+"/",function(data) {
			$(".click_menu[rel="+rel+"]").attr("src","/system/template/img/menu_"+data+".png");
		});
		return false;
	});
});
</script>

<form action="#" method="post">
	
	<fieldset>
		<legend>Структура сайта:</legend>
		<table cellpadding="0" cellspacing="0" class="editor">
			<tr class="header">
				<td>Заголовок страницы</td>
				<td class="center" width="80px">ID</td>
				<td class="center" width="100px">Плагин</td>
				<td class="center" width="120px">Управление</td>
			</tr>
			<?php
			if(count($content) > 0)
			{
				foreach($content as $v)
				{
					if(mb_strlen($v["title"]) > 100) {
						$v["title"] = mb_substr($v["title"],0,100)."...";
					}
					echo '
					<tr>
						<td>
							<a href="/admin/pages/up/'.$v["id"].'/" title="Переместить вверх"><img src="/system/template/img/up.png" /></a>
							<a href="/admin/pages/dn/'.$v["id"].'/" title="Переместить вниз"><img src="/system/template/img/down.png" /></a>
							<a href="/admin/pages/add/'.$v["id"].'/" title="Добавить вложенную страницу"><img src="/system/template/img/addsmall.png" /></a>
							<img src="/system/template/img/menu_'.$v["menu"].'.png" class="click_menu" rel="'.$v["id"].'" title="Включить меню" />
					';
					
					if($v["child"] > 0) {
						if($v["showchild"] == 1) {
							echo '<a href="/admin/pages/showchild/'.$v["id"].'/0/" title="Скрыть внутренние страницы"><img src="/system/template/img/minimize.png" /></a>';
						} else {
							echo '<a href="/admin/pages/showchild/'.$v["id"].'/1/" title="Просмотр внутренних страниц"><img src="/system/template/img/maximize.png" /></a>';
						}
					}
					
					echo '
							<a href="/admin/pages/edit/'.$v["id"].'/" title="">'.$v["title"].'</a>
						</td>
						<td class="center">'.$v["id"].'</td>
					';
						
					if($v["plugin"] == "pages") {
						echo '<td class="center"></td>';
					} else {
						echo '<td class="center"><a href="/admin/'.$v["plugin"].'/" title="Управление данными плагина">'.$v["plugin"].'</a></td>';
					}
						
					echo '
						<td class="control">
						<a href="/'.$v["engname"].'/" target="_blank" title="Просмотр страницы на сайте"><img src="/system/template/img/eye.png" /></a>
					';
					
					if($v["child"] > 0) {
						echo '<img src="/system/template/img/delete.png" style="opacity:0.5; cursor: default;" />';
					} else {
						echo '<a href="/admin/pages/delete/'.$v["id"].'/" onclick="return confirm(\'Вы уверенны, что хотите удалить страницу: '.$v["title"].'?\'); return false;" title="Удалить страницу"><img src="/system/template/img/delete.png" /></a>';
					}
						
					echo '
						<a href="/admin/pages/edit/'.$v["id"].'/" title="Редактировать страницу"><img src="/system/template/img/reply.png" /></a>
						</td>
						</tr>
					';
					
					if($v["child"] > 0 and $v["showchild"] == 1) {
						structure($v["id"]);
					}
				}
			}
			?>
		</table>
	</fieldset>
	<a href="/admin/pages/add/" class="button">Создать новую страницу</a>
</form>