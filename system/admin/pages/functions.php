<?php
if( !defined("BENGINE") ) { die ("Hacking!"); }

#Запрос на добавление
if(isset($nodes[2]) and $nodes[2] == "add" and isset($_POST["submit"]) and count($_POST["submit"]) > 0)
{
	(isset($nodes[3]) and (int)$nodes[3] > 0) ? $parent = $nodes[3] : $parent = 0;
	add("pages", $_POST, "parent='".$parent."'", $parent);
	cacheDel("pages");
	header("Location: /admin/pages/");
	die();
}

#Запрос на редактирование
if(isset($nodes[2]) and $nodes[2] == "edit" and isset($nodes[3]) and (int)$nodes[3] > 0 and isset($_POST["submit"]) and count($_POST["submit"]) > 0)
{
	if(edit("pages", $_POST, $nodes[3]) != true) {
		error('При редактировании страницы возникли ошибки',__FILE__,__LINE__);
		die("При редактировании страницы возникли ошибки");
	}
	cacheDel("pages");
}

#Сворачиваем/разворачиваем подстраницы
if(isset($nodes[2]) and $nodes[2] == "showchild" and isset($nodes[3]) and (int)$nodes[3] > 0)
{
	if(empty($nodes[4])) {
		$nodes[4] = 0;
	}
	if(doquery("UPDATE `pages` SET `showchild`='".$nodes[4]."' WHERE id=".$nodes[3]." LIMIT 1")) {
		header("Location: /admin/pages/");
		die();
	}
	else {
		error('Ошибка работы с дочерними страницами',__FILE__,__LINE__);
		die("Ошибка работы с дочерними страницами");
	}
}

#Перемещение вверх или вниз
if(isset($nodes[2]) and ($nodes[2] == "up" or $nodes[2] == "dn") and isset($nodes[3]) and (int)$nodes[3] > 0)
{
	updown("pages", $nodes[3], $nodes[2], "parent");
	cacheDel("pages");
	header("Location: /admin/pages/"); die();
}

#Включение в меню
if(isset($nodes[2]) and $nodes[2] == "menu" and isset($nodes[3]) and (int)$nodes[3] > 0)
{
	$menu = onoff("pages", $nodes[3], $nodes[2]);
	cacheDel("pages");
	die("".$menu."");
}

#Удаление страницы
if(isset($nodes[2]) and $nodes[2] == "delete" and isset($nodes[3]) and (int)$nodes[3] > 0)
{
	$sql_query = doquery("SELECT `order`,`parent`,`child` FROM `pages` WHERE id='".$nodes[3]."' LIMIT 1");
	if(dorows($sql_query) == 1)
	{
		$sql = doassoc($sql_query);
		if($sql["child"] == 0) {
			doquery("UPDATE `pages` SET `order`=`order`-1 WHERE `parent`='".$sql["parent"]."' and `order`>'".$sql["order"]."'");
		}
		if($sql["parent"] > 0) {
			doquery("UPDATE `pages` SET `child`=`child`-1 WHERE `id`='".$sql["parent"]."' LIMIT 1");
		}
		doquery("DELETE FROM `pages` WHERE id='".$nodes[3]."' LIMIT 1");
		cacheDel("pages");
		header("Location: /admin/pages/"); die();
	} else {
		die("Такая страница не найдена");
	}
}
?>