<?php
if( !defined("BENGINE") ) { die ("Hacking!"); }

if(isset($nodes[0]) and $nodes[0] == "admin") {
	header("Location: /admin/info/");
	die();
}

#Сайт закрыт
if($cfg["offline"] == "0" and (!isset($_SESSION["admin"]) or $_SESSION["admin"] != true))
{
	#503
	$php_sapi_name = php_sapi_name();
	if ($php_sapi_name == 'cgi' or $php_sapi_name == 'cgi-fcgi') {
		header('Status: 503 Service Unavailable');
	} else {
		header($_SERVER['SERVER_PROTOCOL'] . ' 503 Service Unavailable');
	}
	header('Retry-After: 86400');
	$header = "/system/template/null.tpl";
	$body = "/system/template/closed.tpl";
	$footer = "/system/template/null.tpl";
}
else
{
	#Подключение всех активных плагинов
	$active_plugins_query = doquery("SELECT * FROM `config` WHERE `module` != 'system'");
	if(dorows($active_plugins_query) > 0)
	{
		$active_plugins_extend = doarray($active_plugins_query);
		
		#Сортируем все плагины по параметрам расширения и кэширования
		foreach($active_plugins_extend as $v) {
			$active_plugins[$v["module"]][$v["type"]] = $v["value"];
		}
		
		foreach($active_plugins as $k => $v)
		{			
			#Чтение кэшированных данных
			if(isset($v["cache"]) and $v["cache"] == 1)
			{
				$nameCache = "cache_".$k;
				
				#Сохраняем и берем кэш
				if(($$nameCache = cacheGet($k)) === false) {
					if(!isset($v)) {
						$v = array();
					}
					$$nameCache = cacheAdd($k,$v);
				}
				$smarty->assign($nameCache, $$nameCache);
			}
			#Подключение расширения на каждую страницу
			if(isset($v["extend"]) and $v["extend"] == 1 and file_exists(ROOT_DIR."/plugins/".$k."/plugin.php")) {
				$plugin_config = $v;
				include_once(ROOT_DIR."/plugins/".$k."/plugin.php");
			}
		}
	}
	
	#Смотрим, какую страницу запросил пользователь
	if(isset($nodes[0]) and $nodes[0] != "") {
		if(is_numeric($nodes[0])) {
			$where = "`id`='".$nodes[0]."'";
		} else {
			$where = "`engname`='".$nodes[0]."'";
		}
	} else {
		if((int)$cfg["page"] > 0) {
			$where = "`id`='".$cfg["page"]."'";
		}
	}

	#Подключение выбранной страницы
	if(isset($where) and ($sql_pages = doquery("SELECT * FROM `pages` WHERE ".$where." LIMIT 1")) != false and dorows($sql_pages) == 1) {
		$page = doassoc($sql_pages);
		$header = "/template/".$page["header"];
		$body   = "/template/".$page["body"];
		$footer = "/template/".$page["footer"];
	} else {
		#404
		$php_sapi_name = php_sapi_name();
		if ($php_sapi_name == 'cgi' or $php_sapi_name == 'cgi-fcgi') {
			header('Status: 404 Not Found');
		} else {
			header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
		}
		$header = "/system/template/null.tpl";
		$body   = "/template/".$cfg["error404"];
		$footer = "/system/template/null.tpl";
		$page["title"] = "404 Not Found";
	}
	
	#Подключаем плагин
	if(isset($page["plugin"]) and $page["plugin"] != "pages" and $page["plugin"] != "") {
		if(file_exists(ROOT_DIR."/plugins/".$page["plugin"]."/plugin.php")) {
			$pl = $page["plugin"];
			include_once(ROOT_DIR."/plugins/".$page["plugin"]."/config.php");
			include_once(ROOT_DIR."/plugins/".$page["plugin"]."/plugin.php");
		}
	}
}
?>