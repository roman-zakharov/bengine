<?php
if( !defined("BENGINE") ) { die ("Hacking!"); }

# Перенаправление
if($cfg["url"] != $_SERVER["SERVER_NAME"] and IP != "127.0.0.1") {
	header("Location: http://".$cfg["url"].$_SERVER['REQUEST_URI']);
	die("Перенаправление на http://".$cfg["url"].$_SERVER['REQUEST_URI']);
}

#Авторизация
if(!isset($_SESSION["login"]) or !isset($_SESSION["admin"]) or $_SESSION["admin"] == false)
{
	if(isset($_POST["login"]) and isset($_POST["passw"]))
	{
		if($_POST["login"] != "" and $_POST["passw"] != "")
		{
			#Форматируем переменные
			$login = strip_tags($_POST["login"]);
			$passw = md5($_POST["passw"]);
			if(($user = doqueryassoc("SELECT * FROM `users` WHERE `login`='".$login."' and `passw`='".$passw."' LIMIT 1")) != false)
			{
				$usergroup = doqueryassoc("SELECT `admin`, `permission` FROM `groups` WHERE `id`='".$user["group"]."' LIMIT 1");
				if($usergroup["admin"] == 1) {
					#успешная авторизация
					$_SESSION = $user;
					$_SESSION["admin"] = 1;
					$_SESSION["permission"] = unserialize(decode($usergroup["permission"]));
					#обновляем данные с последнем входе
					$post["auht"] = DATETIME;
					$post["ip"] = IP;
					edit("users", $post, $user["id"]); die("1");
				} else {
					die("Вход в панель управления отклонен");
				}
			} else {
				die("Неверно введен логин или пароль.");
			}
		} else {
			die("Заполните логин и пароль администратора.");
		}
	}
	
	#Шаблон для авторизации
	$header = "/system/template/null.tpl";
	$body   = "/system/template/enter.tpl";
	$footer = "/system/template/null.tpl";
}
else 
{
	#Перенаправление, если не обозначена нода
	if(!isset($nodes[1]) or $nodes[1] == "") {
		$nodes[1] = "info";
	}
		
	#Проверяем группу и доступ к разделу
	if($nodes[1] == "info" or isset($_SESSION["permission"][$nodes[1]]) or $_SESSION["root"] == 1)
	{ 
		#Подключение файла для работы с данными
		if(file_exists(ROOT_DIR."/system/admin/".$nodes[1]."/admin.php")) {
			include_once(ROOT_DIR."/system/admin/".$nodes[1]."/admin.php");
		} else {
			if(file_exists(ROOT_DIR."/plugins/".$nodes[1]."/admin.php")) {
				include_once(ROOT_DIR."/plugins/".$nodes[1]."/config.php");
				include_once(ROOT_DIR."/plugins/".$nodes[1]."/admin.php");
			}
		}
	}
	
	#Шаблон раздела
	$header = "/system/template/header.tpl";
	if(!isset($body) or $body == "") {
		$body  = "/system/template/error.tpl";
	}	
	$footer = "/system/template/footer.tpl";
}
?>