<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru"> 
<head>
<title>Bengine :: Панель управления сайтом</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="icon" href="/system/template/img/favicon.ico">
<link rel="stylesheet" href="/system/template/css/normalize.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/system/template/css/style.css" type="text/css" media="screen" />
<!--[if IE 6]><link rel="stylesheet" type="text/css" media="all" href="/system/template/css/ie6.css"  /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" type="text/css" media="all" href="/system/template/css/ie7.css"  /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" type="text/css" media="all" href="/system/template/css/ie8.css"  /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" type="text/css" media="all" href="/system/template/css/ie9.css"  /><![endif]-->
<script type="text/javascript" src="/system/template/js/jquery.js"></script>
<script type="text/javascript" src="/system/template/tinymce/tiny_mce.js"></script>
<script type="text/javascript">
function elFinderBrowser(field_name, url, type, win) {
	var elfinder_url = '/system/template/elfinder/elfinder.html';
	tinyMCE.activeEditor.windowManager.open({
		file: elfinder_url,
		title: 'Bengine File Browser',
		width: 950,  
		height: 500,
		resizable: 'yes',
		inline: 'yes',
		popup_css: false,
		close_previous: 'no',
	},{
		window: win,
		input: field_name
	});
	return false;
}
$(document).ready(function(){
	$("input[type=text]").addClass("text");
	$("input[type=radio]").addClass("radio");
	$("input[type=submit],button").addClass("submit");
	$("input[type=checkbox]").addClass("checkbox");
	$("input[type=password]").addClass("password");
});
</script>
<?php
if(SEO == false) {
	echo '<style>.seo {display: none;}</style>';
}
#Специальные плагины отображаемые в верхнем меню
$plugins_query = doquery("SELECT * FROM `plugins` WHERE `admin`=2 and `install`=1 ORDER BY `title`");
if(dorows($plugins_query) > 0) {
	$navplugins = doarray($plugins_query);
}
?>
</head>
<body>
<div id="load"></div>
<div class="head">
	<div class="logo">
		Bengine CMS
		<div class="logo_menu">
			<a id="update"></a>
			<a href="/" target="_blank">Перейти на сайт</a>
			<a href="?exit">Выход</a>
		</div>
	</div>
	<div class="navigator">
		<div class="navlink<?php if($nodes[1]=='info'){echo "_action";} ?> load"><a href="/admin/info/">Главная</a></div>
		<div class="navlink<?php if($nodes[1]=='pages'){echo "_action";} ?> load"><a href="/admin/pages/">Структура</a></div>
		<div class="navlink<?php if($nodes[1]=='users'){echo "_action";} ?> load"><a href="/admin/users/">Пользователи</a></div>
		<div class="navlink<?php if($nodes[1]=='plugins'){echo "_action";} ?> load"><a href="/admin/plugins/">Плагины</a></div>
		<div class="navlink<?php if($nodes[1]=='config'){echo "_action";} ?> load"><a href="/admin/config/">Конфигурация</a></div>
		<?php
		if(isset($navplugins) and count($navplugins) > 0) {
			echo '<div class="navlink load"><a></a></div>';
			foreach($navplugins as $v){
				($nodes[1] == $v['name']) ? $navlink = "_action" : $navlink = "";
				echo '<div class="navlink'.$navlink.' load"><a href="/admin/'.$v['name'].'/">'.$v['title'].'</a></div>';
			}
		}
		?>
	</div>
</div>
<div class="body">