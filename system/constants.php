<?php
/* Bengine */
if(!defined("BENGINE")) {
	define("BENGINE", true);
}

/* мемкэш */
if(!defined("MEMCACHE")) {
	define("MEMCACHE", false);
}

/* максимальный лимит записей для кэша */
if(!defined("CACHE_LIMIT")) {
	define("CACHE_LIMIT", 1000);
}

/* дата-время */
if(!defined("DATETIME")) {
	define("DATETIME", date("Y-m-d H:i:s"));
}

/* проверка обновления системы */
if(!defined("UPDATE")) {
	define("UPDATE", false);
}

/* IP клиента */
if(!defined("IP")) {
	if(isset($_SERVER["HTTP_X_REAL_IP"]) and $_SERVER["HTTP_X_REAL_IP"] != "") {
		define("IP", $_SERVER["HTTP_X_REAL_IP"]);
	} else {
		define("IP", $_SERVER["REMOTE_ADDR"]);
	}
}

/* Посещаемая страница */
if(!defined("URL")) {
	if(isset($_SERVER["REDIRECT_URL"]) and $_SERVER["REDIRECT_URL"] != "") {
		$_SERVER["REDIRECT_URL"] = htmlspecialchars($_SERVER["REDIRECT_URL"], ENT_QUOTES, "UTF-8");
		define("URL", $_SERVER["REDIRECT_URL"]);
	} else {
		if(isset($_SERVER["REQUEST_URI"]) and $_SERVER["REQUEST_URI"] != "") {
			$_SERVER["REQUEST_URI"] = htmlspecialchars($_SERVER["REQUEST_URI"], ENT_QUOTES, "UTF-8");
			define("URL", $_SERVER["REQUEST_URI"]);
		} else {
			define("URL", "/");
		}
	}
}

/* Опции SEO для страниц */
if(!defined("SEO")) {
	define("SEO", false);
}

?>