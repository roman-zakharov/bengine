<?php

function bengine_seo_mysql_config() {	
	$sql = doquery("SELECT * FROM config");
	$tmp_cfg = doarray($sql);
	foreach($tmp_cfg as $v) {
		if($v["module"] == "system") {
			$cfg[$v["type"]] = $v["value"];
		}
	}
	return $cfg;
}

function bengine_seo_robots_txt()
{
	header("Content-type: text/plain");
	$cfg = bengine_seo_mysql_config();
	
	$robots = "User-agent: *\nDisallow: /admin\nSitemap: http://".$cfg["url"]."/sitemap.xml\nHost: ".$cfg["url"];	
	if(file_exists(ROOT_DIR."/robots.txt")) {
		$robots = file_get_contents(ROOT_DIR."/robots.txt");
	}	
	echo $robots;
	die();
}

function bengine_seo_sitemap_xml()
{
	header("Content-type: application/xml");
	$cfg = bengine_seo_mysql_config();
	
	$sitemap = '<?xml version="1.0" encoding="UTF-8"?>
	<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
	http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
	<url> 
		<loc>http://'.$cfg["url"].'/</loc> 
		<changefreq>daily</changefreq> 
		<priority>1.0</priority>
	</url>
	';
	
	if(cacheGet("pages") == false) {
		cacheAdd("pages");
	}
	$cache_pages = cacheGet("pages");
	
	if(count($cache_pages) > 0) {
		foreach($cache_pages as $v)
		{
			if($v["menu"] == 1 and $v["id"] != $cfg["page"])
			{
				($v["parent"] == 0) ? $parent = '0.80' : $parent = '0.64';
				$sitemap .= '
				<url>
				<loc>http://'.$cfg["url"].'/'.$v["engname"].'/</loc>
				<changefreq>weekly</changefreq>
				<priority>'.$parent.'</priority>
				</url>
				';
			}
		}
	}
	
	$sitemap .= '</urlset>';
	
	if(file_exists(ROOT_DIR."/sitemap.xml")) {
		$sitemap = file_get_contents(ROOT_DIR."/sitemap.xml");
	}
	echo $sitemap;
	die();
}

#Вывод на экран
if(isset($_GET["seo"]) and $_GET["seo"] != "") {
	if($_GET["seo"] == "robots") {
		bengine_seo_robots_txt();
	}
	if($_GET["seo"] == "sitemap") {
		bengine_seo_sitemap_xml();
	}
}

?>