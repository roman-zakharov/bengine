<?php

#выход
if(isset($_GET["exit"]))
{
	#print_r($_SESSION); die();
	session_unset();
	if(session_destroy() == false) {
		unset($_SESSION["login"]);
		unset($_SESSION["admin"]);
	}
	setcookie("login","",time()-3600);
	setcookie("passw","",time()-3600);
	header("Location: /");
	exit();
}

# Запись ошибок в файл
# Использование error('Сообщение',__FILE__,__LINE__,__FUNCTION__);
function error($text, $file = "", $line = "", $function = "") {
	if($file != "") {
		$file = ", ".$file."";
	}
	if($line != "") {
		$line = ":".$line." ";
	}
	if($function != "") {
		$function = $function."()";
	}
	$error = IP." [".DATETIME."] ".$text.$file.$line.$function;
	$file = ROOT_DIR."/.error";
	
	if(($text = file_get_contents($file)) == false) {
		if(@file_put_contents($file,$error)) {
			return true;
		}
	} else {
		if(@file_put_contents($file,$text."\n".$error)) {
			return true;
		}
	}
	return false;
}

#htmlspecialchars
function encode($text) {
	return htmlspecialchars(trim($text), ENT_QUOTES, "UTF-8");
}

#htmlspecialchars_decode
function decode($text) {
	return htmlspecialchars_decode($text, ENT_QUOTES);
}

#Вывод значения переменных на экран
function mpr($text)
{
	if((isset($_SESSION["root"]) and $_SESSION["root"] == 1) or IP == "127.0.0.1") {
		echo "<pre style='text-align: left;'>";
		print_r($text);
		echo "</pre>";
	}
}

function mvd($text)
{
	if((isset($_SESSION["root"]) and $_SESSION["root"] == 1) or IP == "127.0.0.1") {
		echo "<pre style='text-align: left;'>";
		var_dump($text);
		echo "</pre>";
	}
}

#Обработка строки запроса браузера
function nodes()
{
	$nodes = array();	
	$url = str_replace($_SERVER["SCRIPT_NAME"],"",URL);
	
	if(!empty($url) and $url != "/" and file_exists(ROOT_DIR.$url) and is_file(ROOT_DIR.$url) and $url != "index.php") {
		if(($cont = file_get_contents(ROOT_DIR.$url)) != "") {
			include_once(ROOT_DIR.$url);
			die();
		}
	}
	
	$get = "";
	if($_SERVER["QUERY_STRING"] != "") {
		foreach($_GET as $k => $v) {
			# массив
			if(is_array($v)) {
				foreach($v as $val) {
					$get .= $k."%5B%5D=".$val."&";
				}
			}
			# пустое значение
			elseif($v == '') {
				$get .= $k."=&";
			}
			# любое значение
			else {
				$get .= $k."=".$v."&";
			}
		}
	}
	
	$nodes_url = explode('/',$url);
	$nodes = array(
		"cfg" => array(
			"count" => 0,
			"script" => $_SERVER["SCRIPT_NAME"],
			"get" => decode($get),
			"href" => "/",
			"prev" => "/",
			"end" => ""
		)
	);
	
	foreach($nodes_url as $v)
	{
		if(!empty($v) and decode($v) != "?".$_SERVER["QUERY_STRING"])
		{
			$v = bengine_chars($v);
			$nodes[] = str_replace(".html", "", $v);
			$nodes["cfg"]["href"] = $nodes["cfg"]["href"].$v."/";
		}
	}
	
	#Подсчет количества nodes
	$count = count($nodes)-1;
	
	#предыдущая страница и последняя nodes
	if($count > 0)
	{
		$nodes["cfg"]["end"] = $nodes[$count-1];
		$nodes["cfg"]["count"] = $count;
		
		if(preg_match("/\/$/",$nodes["cfg"]["href"]) == false) {
			$nodes["cfg"]["href"] = $nodes["cfg"]["href"]."/";
		}
		for($i=0;$i<=$count-2;$i++) {
			$nodes["cfg"]["prev"] .= $nodes[$i]."/";
		}
	}
		
	return $nodes;
}

# Пагинация
# $view - сколько страниц показывать
function paginator($view)
{
	global $cfg, $nav, $p;
	$result = array();
	
	# коэффициент отклонения влево вправо
	$pageKoff = round($view/2);
	
	# Всего страниц
	$countList = count($nav["list"]);
	
	# Начало отсчета 
	$startPage = 1;
	$startPageKoff = $pageKoff - $p;
	if($p > $pageKoff) {
		$startPage = $p - $pageKoff;
		$startPageKoff = -1;
	}
	
	# Конец отсчета
	$finishPage = $p + $pageKoff + $startPageKoff;
	if($finishPage > $countList) {
		$finishPage = $countList;
	}
	
	# чтобы было красиво
	if($view > $countList - $finishPage) {
		$addSstartPage = $finishPage - $startPage - $view + 1;
		if($startPage + $addSstartPage > 0) {
			$startPage = $startPage + $addSstartPage;
		}
	}
	
	# отдаем массив
	for($i=$startPage; $i<=$finishPage; $i++) {
		$result[] = $i;
	}
	return $result;
}

#Удаляем символы
function bengine_strip_tags($text)
{
	$text = trim($text);
	$text = strip_tags($text);
	$in = array("  "," ","!","@","#","$","%","^","&","*","-","`","~",",","/","\\",">","<",";","№","+","|","[","]","{","}","(",")","\"",":","?","'","«","»",
	"À", "Á", "Â", "Ã", "Ä", "Å", "Æ", "Ç", "È", "É", "Ê", "Ë", "Ì", "Í", "Î", "Ï",
	"Ð", "Ñ", "Ò", "Ó", "Ô", "Õ", "Ö", "Ø", "Ù", "Ú", "Û", "Ü", "Ý", "А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж",
	"З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ъ",
	"Ь", "Э", "Ю", "Я");
	$ou = array("", "_", "", "", "", "", "", "", "", "", "", "", "", "", "", "" , "", "", "", "", "", "", "", "", "", "", "", "",  "", "", "", "", "", "",
	"à", "á", "â", "ã", "ä", "å", "æ", "ç", "è", "é", "ê", "ë", "ì", "í", "î", "ï",
	"ð", "ñ", "ò", "ó", "ô", "õ", "ö", "ø", "ù", "ú", "û", "ü", "ý", "а", "б", "в", "г", "д", "е", "ё", "ж",
	"з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы",
	"ь", "э", "ю", "я");
	$text = str_replace($in, $ou, $text);
	return $text;
}

#Удаляем символы
function bengine_chars($text)
{
	$text = trim($text);
	$in = array("\"","`","'","\\","<",">","′","’");
	$ou = array("&quot;","&#096;","&#039;","\\\\","&lt;","&gt;","&prime;","&rsquo;");
	$text = str_replace($in, $ou, $text);
	return $text;
}

#Перевод в транслит
function translit($text)
{
	$text = mb_strtolower($text);
	$text = trim($text);
	$text = strip_tags($text);
	$arr = array(
		'дж' => 'j' , ' ' => '_',
		'а' => 'a' , 'б' => 'b'  , 'в' => 'v' , 'г' => 'g', 'д' => 'd', 
		'е' => 'e' , 'ё' => 'yo' , 'ж' => 'zh', 'з' => 'z', 'и' => 'i', 
		'й' => 'i', 'к' => 'k'  , 'л' => 'l' , 'м' => 'm', 'н' => 'n', 
		'о' => 'o' , 'п' => 'p'  , 'р' => 'r' , 'с' => 's', 'т' => 't', 
		'у' => 'u' , 'ф' => 'f'  , 'х' => 'h', 'ц' => 'ts', 'ч' => 'ch', 
		'ш' => 'sh', 'щ' => 'shc', 'ъ' => '' , 'ы' => 'y', 'ь' => '', 
		'э' => 'e', 'ю' => 'yu' , 'я' => 'ya'
	);
	$in = array_keys($arr);
    $ou = array_values($arr);
    $text = str_replace($in,$ou,$text);
    $text = str_replace("__","_",$text);
	$text = preg_replace("/[^A-z0-9._\s]/", "", $text);
	return $text;
}

#Перевод в транслит
function translate($text){
	return translit($text);
}

#Генератор случайных данных
function generator($in,$ou)
{
	$arrsymbol = array(
		'1','2','3','4','5','6','7','8','9',
		'Q','W','E','R','T','Y','U','P','A','S','D','F','G','H','J','K','Z','X','C','V','B','N','M',
		'q','w','e','r','t','y','u','p','a','s','d','f','g','h','j','k','z','x','c','v','b','n','m'
	);
	
	$pasymbol = "";
	$simbol_rand = rand($in,$ou);	
	$count_arrsymbol = count($arrsymbol);
	
	for($i = 0; $i <= $simbol_rand; $i++)
	{
		$indexbot = rand(0, $count_arrsymbol - 1);
		$pasymbol .= $arrsymbol[$indexbot];
	}
	return $pasymbol;
}

#Отправка почты
function bengine_mail($to, $title, $msg, $f = false)
{
	global $cfg;
	
	$title = substr($title,0,100);
	
	if($f == false) {
		$from_mail = explode(",",$cfg["email"]);
		$from = $from_mail[0];
	} else {
		$from = $f;
	}
	
	$headers  = "From: =?utf-8?B?".base64_encode($cfg["title"])."?= <".$from."> \r\n";
	$headers .= "MIME-Version: 1.0 \r\n";
	$headers .= "Date: ".date("r", time())." \r\n";
	$headers .= "Content-Type: text/html; charset=UTF-8 \r\n";
	$headers .= "Content-Transfer-Encoding: 8bit \r\n";
	$headers .= "X-PHP-Originating-Script: BengineMail \r\n";
	$headers .= "X-Priority: 3 \r\n";
	$headers .= "X-MSMail-Priority: Normal \r\n";
	$headers .= "X-Mailer: Bengine ".$cfg["vers"]." \r\n";
	$headers .= "X-MimeOLE: Bengine ".$cfg["vers"]." \r\n";
	$headers .= "X-AntiAbuse: Servername - ".$_SERVER["SERVER_NAME"]." (".$_SERVER["SERVER_ADDR"].") \r\n";
	
	if(@mail($to, $title, $msg, $headers)) {
		return true;
	}
	else {
		return false;
	}
}

/**
* Включение и отключение параметра записи в БД
* 
* @param string $table таблица содержащая значение
* @param int $id номер строки содержащий параметр
* @param string $param параметр, который необходимо изменить
* @return новое значение параметра
* 
*/
function onoff($table, $id, $param) 
{
	#Смотрим параметр
	$query = doquery("SELECT `".$param."` FROM `".$table."` WHERE id = ".$id." LIMIT 1");
	if($query == false or dorows($query) == 0) {
		return false;
	} else {
		$row = doassoc($query);
		$result = $row[$param];
	}
	
	
	if(is_numeric($result)) {
		$result == 1 ? $val = 0 : $val = 1;	
	} else {
		return false;
	}
	
	#Обновляем данные
	$update = "
	UPDATE
		`".$table."`
	SET
		`".$param."` = ".$val."
	WHERE
		id = ".$id."
	LIMIT 1";
	
	if(!doquery($update)) {
		return false;
	}
	
	return $val;
}

/**
* Перемещение строки таблицы вверх или вниз
* 
* @param string $table таблица содержащая значение
* @param int $id номер строки содержащий параметр
* @param string $param значение перемещения вниз (dn) или вверх (up)
* @param string $row совпадение элементов в таблице по столбцу
* @return true or false
* 
*/
function updown($table, $id, $param, $row = false) 
{
	#Смотрим параметр
	$query = doquery("SELECT * FROM `".$table."` WHERE id = ".$id." LIMIT 1");
	if($query == false or dorows($query) == 0) {
		return false;
	} else {
		$sql = doassoc($query);
		$order1 = $sql["order"];
	}
	
	if	  ($param == "up") 	{ $order2 = $order1 - 1; }
	elseif($param == "dn") 	{ $order2 = $order1 + 1; }
	else 					{ return false; }
	
	if($row != false) {
		$where = "WHERE t1.".$row."='".$sql[$row]."' and t2.".$row."='".$sql[$row]."'";
	} else {
		$where = "";
	}
	
	#Обновляем данные
	$update = "
	UPDATE
		`".$table."` AS t1 INNER JOIN `".$table."` AS t2
		ON
		t1.order = ".$order2."
		and
		t2.order = ".$order1."
	SET
		t1.order = ".$order1.",
		t2.order = ".$order2."
	".$where."
	";
	
	if(!doquery($update)) {
		return false;
	}
	
	return true;
}

#
#ФУНКЦИИ ДЛЯ РАБОТЫ С ПЛАГИНАМИ
#

#Выводит список файлов шаблона системы
function selected($array, $search)
{
	global $cfg, $content, $plugin_config;
	
	#Сравниваем все шаблоны с запрошенным
	if(isset($array) and is_array($array) and count($array) > 0)
	{
		$options = '<select name="'.$search.'">';
		foreach($array as $k => $v)
		{
			if(is_array($v)) {
				return false;
			}
			#Ищем подходящий шаблон для контента
			if(isset($content[$search]) and $content[$search] == $v) {
				$options .= '<option value="'.$v.'" selected="selected">'.$v.'</option>';
			} else {
				#Ищем подходящий шаблон для плагина
				if(isset($plugin_config[$search]) and $plugin_config[$search] == $v) {
					$options .= '<option value="'.$v.'" selected="selected">'.$v.'</option>';
				} else {
					#Ищем подходящий шаблон для системы
					if(!isset($content[$search]) and isset($cfg[$search]) and $cfg[$search] == $v) {
						$options .= '<option value="'.$v.'" selected="selected">'.$v.'</option>';
					} else {
						$options .= '<option value="'.$v.'">'.$v.'</option>';
					}
				}
			}
		}
		$options .= '</select>';
	}
	else
	{
		return false;
	}
	return $options;
}

#Добавление контента в таблицу
function add($table, $post = array(), $order = false, $parent = 0)
{
	global $db, $page;
	
	if(isset($post["id"]) and is_numeric($post["id"])) {
		$insert = "id='".$post["id"]."'";
	} else {
		$insert = "id=NULL";
	}
	
	#Проверяем входящие данные
	if(!is_array($post) or count($post) == 0) {
		return false;
	}
	
	#Обрабатываем данные запроса
	foreach($post as $k => $v) {
		if(!is_array($post[$k])) {
			$post[$k] = htmlspecialchars(trim($v), ENT_QUOTES, "UTF-8");
		}
		#значения по умолчанию
		if($k == "datetime" and $post[$k] == "") {
			$post[$k] = date("Y.m.d H:i:s");
		}
	}
	
	#Родительская страница
	$post["parent"] = $parent;
	
	#Сортировка, если нужна
	if($order != false) {
		$post["order"] = docount($table, $order)+1;
	} else {
		$post["order"] = docount($table)+1;
	}
	
	#Заголовок транслитом
	if(isset($post["engname"])) {
		if($post["engname"] == "") {
			if(isset($post["title"]) and $post["title"] != "") {
				$post["engname"] = translit(bengine_strip_tags($post["title"]));
			} else {
				return false;
			}
		} else {
			$post["engname"] = translit(bengine_strip_tags($post["engname"]));
		}
	}
	
	#Смотрим таблицу
	$t = dodescribe($table);
	
	#Проверяем поля таблицы
	if(!is_array($t) or count($t) == 0) {
		return false;
	} else {
		if(isset($t["id"])) {
			unset($t["id"]);
		}
		#Не указана дата
		if(isset($t["datetime"]) and $t["datetime"] and !isset($post["datetime"])) {
			$post["datetime"] = date("Y.m.d H:i:s");
		}
		#Нужен логин
		if(isset($t["login"]) and !isset($post["login"]) and isset($_SESSION["login"])) {
			$post["login"] = $_SESSION["login"];
		}
		#Не указана страница
		if(isset($t["page"]) and $t["page"] and !isset($post["page"]) and isset($page["id"])) {
			$post["page"] = $page["id"];
		}
	}
	
	#Присваеваем каждому полю таблицы значение
	foreach($t as $k => $v) {
		if(isset($post[$k]) and $post[$k] != "") {
			if(is_array($post[$k])) {
				$_tmp_post = htmlspecialchars(serialize($post[$k]), ENT_QUOTES, "UTF-8");
				$insert .= ", `".$k."`='".$_tmp_post."'";
				unset($_tmp_post);
			} else {
				$insert .= ", `".$k."`='".$post[$k]."'";
			}
		} else {
			if($v == "int" or $v == "tinyint" or $v == "smallint" or $v == "mediumint" or $v == "bigint") {
				(isset($post[$k]) and $post[$k] > 0) ? $r = $post[$k] : $r = 0;
				$insert .= ", `".$k."`=".$r."";
			}
			elseif($v == "datetime") {
				$insert .= ", `".$k."`='0000-00-00 00:00:00'";
			}
			else {
				$insert .= ", `".$k."`=''";
			}
		}
	}
	
	#Делаем запрос в БД
	if(!doquery("INSERT INTO `".$table."` SET ".$insert." ")) {
		error('Ошибка добавления контента в БД: '.mysqli_error($db).'',__FILE__,__LINE__);
		return false;
	} else {
		if($parent > 0 and isset($t["child"])) {
			doquery("UPDATE `".$table."` SET `child`=`child`+1 WHERE id='".$parent."' LIMIT 1");
		}
		return true;
	}
}

#Редактирование контента в таблице
function edit($table, $post = array(), $id = 0)
{
	global $db;
	
	$update = "UPDATE `".$table."` SET";
	
	#Не может быть записано, не найден идентификатор строки
	if($id == 0) {
		return false;
	}
	
	#Проверяем входящие данные
	if(!is_array($post) or count($post) == 0) {
		return false;
	}
	
	#Обрабатываем данные запроса
	foreach($post as $k => $v) {
		if(!is_array($post[$k])) {
			$post[$k] = htmlspecialchars(trim($v), ENT_QUOTES, "UTF-8");
		}
	}
	
	#Заголовок транслитом !@уменьшить названия функций
	if(isset($post["engname"])) {
		if($post["engname"] == "") {
			if(isset($post["title"]) and $post["title"] != "") {
				$post["engname"] = translit(bengine_strip_tags($post["title"]));
			} else {
				return false;
			}
		} else {
			$post["engname"] = translit(bengine_strip_tags($post["engname"]));
		}
	}
	
	# Время правки записи, если нужно
	if(!isset($post["engname"])) {
		$post["engname"] = DATETIME;
	}
	
	#Смотрим таблицу
	$t = dodescribe($table);
	
	#Проверяем поля таблицы
	if(!is_array($t) or count($t) == 0) {
		return false;
	}
	
	#Создаем запрос
	foreach($post as $k => $v) {
		if(isset($t[$k])) {
			if($t[$k] != "") {
				if(is_array($post[$k])) {
					$_tmp_post = htmlspecialchars(serialize($post[$k]), ENT_QUOTES, "UTF-8");
					$update .= " `".$k."`='".$_tmp_post."',";
					unset($_tmp_post);
				} else {
					$update .= " `".$k."`='".$post[$k]."',";
				}
			} else {
				if($t[$k] == "int" or $t[$k] == "tinyint" or $t[$k] == "smallint" or $t[$k] == "mediumint" or $t[$k] == "bigint") {
					$update .= " `".$k."`=0,";
				}
				elseif($v == "datetime") {
					$insert .= ", `".$k."`='0000-00-00 00:00:00'";
				}
				else {
					$update .= " `".$k."`='',";
				}
			}
		}
	}
	
	$update = substr($update,0,-1);
	
	#Делаем запрос в БД
	if(!doquery($update." WHERE id='".$id."' LIMIT 1")) {
		error('Ошибка редактирования контента в БД: '.mysqli_error($db).'',__FILE__,__LINE__);
		return false;
	} else {
		return true;
	}
}

#Создание таблиц плагинов для утановки
function table_create($plugin, $colums)
{
	foreach($colums as $n => $t)
	{
		$key = "";
		$query = "";
		foreach($t as $k => $v)
		{	
			#Значение по умолчанию
			if(!isset($v["default"])) {
				$v["default"] = "";
			}
			# Цифровое значение
			if($v["type"] == "int" or $v["type"] == "tinyint") {
				if($v["type"] == "int") { $int = "int(10)"; }
				if($v["type"] == "tinyint") { $int = "tinyint(3)"; }
				$query .= "`".$v["name"]."` ".$int." NOT NULL DEFAULT '".$v["default"]."',";
				$key .= "KEY `".$v["name"]."` (`".$v["name"]."`),";
			}
			# Текстовое поле
			elseif($v["type"] == "char" or $v["type"] == "varchar") {
				$query .= "`".$v["name"]."` ".$v["type"]."(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '".$v["default"]."',";
				$key .= "KEY `".$v["name"]."` (`".$v["name"]."`),";
			}
			# Текст
			elseif($v["type"] == "text" or $v["type"] == "londtext") {
				$query .= "`".$v["name"]."` ".$v["type"]." COLLATE utf8_unicode_ci NOT NULL,";
				$key .= "FULLTEXT KEY `".$v["name"]."` (`".$v["name"]."`),";
			}
			# Дата и время
			elseif($v["type"] == "date" or $v["type"] == "datetime") {
				if(!isset($v["default"]) or $v["default"] == "") { $v["default"] = "0000-00-00"; }
				$query .= "`".$v["name"]."` ".$v["type"]." NOT NULL DEFAULT '".$v["default"]."',";
				$key .= "KEY `".$v["name"]."` (`".$v["name"]."`),";
			}
			# Выбор из списка
			elseif($v["type"] == "select") {
				if(!isset($v["default"]) or !is_numeric($v["default"])) { $v["default"] = 0; }
				$query .= "`".$v["name"]."` int(10) NOT NULL DEFAULT '".$v["default"]."',";
				$key .= "KEY `".$v["name"]."` (`".$v["name"]."`),";
			}
			# Флажок
			elseif($v["type"] == "checkbox") {
				if(!isset($v["default"]) or !is_numeric($v["default"])) { $v["default"] = 0; }
				$query .= "`".$v["name"]."` tinyint(1) NOT NULL DEFAULT '".$v["default"]."',";
				$key .= "KEY `".$v["name"]."` (`".$v["name"]."`),";
			}
		}
		
		if(!empty($plugin["header"])) {
			$key .= "FULLTEXT KEY `header` (`header`),";
			$query .= "`header` char(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '".$plugin["header"]."',";
		}
		if(!empty($plugin["body"])) {
			$key .= "FULLTEXT KEY `body` (`body`),";
			$query .= "`body` char(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '".$plugin["body"]."',";
		}
		if(!empty($plugin["footer"])) {
			$key .= "FULLTEXT KEY `footer` (`footer`),";
			$query .= "`footer` char(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '".$plugin["footer"]."',";
		}
		
		$sql[] = "
		CREATE TABLE IF NOT EXISTS `".$n."` (
			`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`page` int(10) unsigned NOT NULL DEFAULT '0',
			`order` int(10) unsigned NOT NULL DEFAULT '0',
			`menu` tinyint(1) unsigned NOT NULL DEFAULT '0',
			`datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
			`title` char(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
			`description` char(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
			`keywords` char(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
			".$query."
			PRIMARY KEY (`id`),
			KEY `page` (`page`),
			KEY `order` (`order`),
			KEY `menu` (`menu`),
			KEY `datetime` (`datetime`),
			FULLTEXT KEY `title` (`title`),
			FULLTEXT KEY `description` (`description`),
			FULLTEXT KEY `keywords` (`keywords`),
			".mb_substr($key,0,-1)."
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;
		";
	}
	return $sql;
}

#Добавление или удаление колонок в созданной бд
function table_colums($colums)
{
	# После какой колонки добавлять новую колонку
	$after = "keywords";
	
	# Добавление новых колонок
	foreach($colums as $table => $c)
	{
		foreach($c as $k => $v)
		{
			if(!doquery("SELECT `".$v["name"]."` FROM ".$table." WHERE 1"))
			{
				#Значение по умолчанию
				if(!isset($v["default"])) {
					if($v["type"] == 'int' or $v["type"] == 'tinyint' or $v["type"] == 'checkbox') {
						$v["default"] = 0;
					}
					elseif($v["type"] == 'date' or $v["type"] == 'datetime') {
						$v["default"] = '0000-00-00 00:00:00';
					}
					else {
						$v["default"] = '';
					}
				}
				# Цифровое значение
				if($v["type"] == "int" or $v["type"] == "tinyint") {
					if($v["type"] == "int") { $int = "int(10)"; }
					if($v["type"] == "tinyint") { $int = "tinyint(3)"; }
					$sql[] = "ALTER TABLE `".$table."` ADD `".$v["name"]."` ".$int." NOT NULL DEFAULT '".$v["default"]."' AFTER `".$after."`";
					$sql[] = "ALTER TABLE `".$table."` ADD INDEX (`".$v["name"]."`)";
				}
				# Текстовое поле
				elseif($v["type"] == "char" or $v["type"] == "varchar") {			
					$sql[] = "ALTER TABLE `".$table."` ADD `".$v["name"]."` ".$v["type"]."(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '".$v["default"]."' AFTER `".$after."`";
					$sql[] = "ALTER TABLE `".$table."` ADD FULLTEXT (`".$v["name"]."`)";
				}
				# Текст
				elseif($v["type"] == "text" or $v["type"] == "londtext") {
					$sql[] = "ALTER TABLE `".$table."` ADD `".$v["name"]."` ".$v["type"]." CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `".$after."`";
					$sql[] = "ALTER TABLE `".$table."` ADD FULLTEXT (`".$v["name"]."`)";
				}
				# Дата и время
				elseif($v["type"] == "date" or $v["type"] == "datetime") {
					$sql[] = "ALTER TABLE `".$table."` ADD `".$v["name"]."` ".$v["type"]." NOT NULL DEFAULT '".$v["default"]."' AFTER `".$after."`";
					$sql[] = "ALTER TABLE `".$table."` ADD INDEX (`".$v["name"]."`)";
				}
				# Выбор из списка
				elseif($v["type"] == "select") {
					if(is_numeric($v["default"])) {
						$sql[] = "ALTER TABLE `".$table."` ADD `".$v["name"]."` int(10) NOT NULL DEFAULT '".$v["default"]."' AFTER `".$after."`";
						$sql[] = "ALTER TABLE `".$table."` ADD INDEX (`".$v["name"]."`)";
					} else {
						$sql[] = "ALTER TABLE `".$table."` ADD `".$v["name"]."` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '".$v["default"]."' AFTER `".$after."`";
						$sql[] = "ALTER TABLE `".$table."` ADD FULLTEXT (`".$v["name"]."`)";
					}
				}
				# Выбор из списка
				elseif($v["type"] == "checkbox") {
					$sql[] = "ALTER TABLE `".$table."` ADD `".$v["name"]."` tinyint(1) NOT NULL DEFAULT '".$v["default"]."' AFTER `".$after."`";
					$sql[] = "ALTER TABLE `".$table."` ADD INDEX (`".$v["name"]."`)";
				}
				$col[] = $v["name"];
				$after = $v["name"];
			}
			$col[] = $v["name"];
			$after = $v["name"];
		}
	}
		
	# Удаление ненужных колонок
	foreach($colums as $table => $c)
	{
		$t = array();
		
		#Смотрим таблицу
		if(($query = doquery("SHOW COLUMNS FROM `".$table."`")) != false)
		{
			while($r = dofetch($query))
			{
				if(isset($r[0]) and
					$r[0] != "id" and
					$r[0] != "page" and
					$r[0] != "order" and
					$r[0] != "menu" and
					$r[0] != "datetime" and
					$r[0] != "title" and
					$r[0] != "description" and
					$r[0] != "keywords"
				){
					$t[] = $r[0];
				}
			}
		}
		
		#Проверяем поля таблицы
		if(is_array($t) and count($t) > 0)
		{		
			foreach($t as $v)
			{
				if(!in_array($v,$col)) {
					$sql[] = "ALTER TABLE `".$table."` DROP `".$v."`";
				}
			}
		}
	}
	
	if(!empty($sql)) {
		return $sql;
	} else {
		return false;
	}
}
?>