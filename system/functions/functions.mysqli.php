<?php

#Подключение к БД
if(!isset($host) or count($host) == 0){
	exit("Настройте доступ к БД в файле system/config.php!");
}

#Проверка расширения mysqli
if(function_exists("mysqli_connect") == false) {
	die("Отсутствует поддержка драйвера mysqli. Работа системы невозможна.");
}

$db = @mysqli_connect($host["host"], $host["user"], $host["pass"], $host["base"]);
if(mysqli_connect_errno()){
	exit("Настройте доступ к БД!");
} else {
	mysqli_query($db,"SET NAMES UTF8");
}

#Запрос
function doquery($query)
{
	global $db;
	$query = mysqli_query($db,$query);
	return $query;
}

#Возврат массива
function dokeyval($query)
{
	$return = array();
	while($rows = mysqli_fetch_assoc($query)) {
		$array[] = $rows;
	}
	if(count($array) > 0) {
		foreach($array as $v) {
			if(isset($v["type"]) and isset($v["value"])) {
				$return[$v["type"]] = $v["value"];
			} else {
				if(isset($v["name"]) and isset($v["title"])) {
					$return[$v["name"]] = $v["title"];
				}
			}
		}
	}
	return $return;
}

#Возврат массива
function doarray($query)
{
	$return = array();
	while($rows = mysqli_fetch_assoc($query)) {
		$return[] = $rows;
	}
	return $return;
}

#Возврат массива
function doassoc($query)
{
	$query = mysqli_fetch_assoc($query);
	return $query;
}

#Количество строк
function dorows($query)
{
	global $db;
	if($query != false and ($query = mysqli_num_rows($query)) == false) {
		$query = mysqli_error($db);
	}
	return $query;
}

#запрос и возврат массива
function doqueryassoc($query)
{
	if(($sql_query = doquery($query)) != false) {
		if(dorows($sql_query) > 0) {
			$return = doassoc($sql_query);
		} else {
			return false;
		}
	} else {
		return false;
	}
	return $return;
}

#запрос и возврат массива
function doqueryarray($query)
{
	if(($sql_query = doquery($query)) != false) {
		if(dorows($sql_query) > 0) {
			$return = doarray($sql_query);
		} else {
			return false;
		}
	} else {
		return false;
	}
	return $return;
}

function dofetch($query)
{
	$query = mysqli_fetch_row($query);
	return $query;
}

# Последний добавленный элемент
function dolast($table, $where = false)
{
	if($where == false) {
		$where = "";
	} else {
		$where = "WHERE ".$where;
	}
	if(($query = doquery("SELECT id FROM ".$table." ".$where." ORDER BY id DESC LIMIT 1")) != false)
	{
		if(dorows($query) == 1) {
			$r = doassoc($query);
			return $r["id"];
		} else {
			return false;
		}
	} else {
		return false;
	}
}

#запрос и возврат массива
function dodescribe($table = false)
{
    $rows = array();
	if($table != false and ($query = doquery("DESCRIBE ".$table)) != false)
	{
		$in = array("0","1","2","3","4","5","6","7","8","9","(",")");
		$ou = array("","","","","","","","","","","","");
		
		while($field = dofetch($query))
		{
			$type = str_replace($in,$ou,$field[1]);
			$type = explode(" ",$type);
			$rows[$field[0]] = $type[0];
		}
	}
	else
	{
		$query = doquery("SHOW TABLES");
		while($r = dofetch($query))
		{
			$rows[] = $r[0];
		}
	}
	return $rows;
}

#Проверка существования таблицы
function doistbl($table)
{
	if(doquery("SELECT 1 FROM `".$table."` WHERE 0") != false) {
		return true;
	} else {
		return false;
	}
}

#Просмотр всех записей таблицы
function dotable($table, $where = "", $limit = "", $order = "")
{
	$where != "" ? $where = "WHERE ".$where : $where = "";
	$order != "" ? $order = "ORDER BY ".$order : $order = "";
	$limit != "" ? $limit = "LIMIT ".$limit : $limit = "";
	
	if(($query = doquery("SELECT * FROM ".$table." ".$where." ".$limit."")) != false)
	{
		if(($rows = doarray($query)) != false)
		{
			return $rows;
		}
	}
	return false;
}

#Количество записей
function docount($table, $where = false)
{
	if($where == false) {
		$where = "";
	} else {
		$where = "WHERE ".$where;
	}
	if(($query = doquery("SELECT COUNT(id) FROM ".$table." ".$where."")) != false)
	{
		if(($query = dofetch($query)) != false) {
			return $query[0];
		} else {
			return false;
		}
	} else {
		return false;
	}
}

#Размер БД
function dosize($dbsize=0)
{
	$return = doquery("SHOW TABLE STATUS");	
    while($row = doassoc($return))
	{  	
        $dbsize += $row["Data_length"] + $row["Index_length"];
    }
	return $dbsize;
}

#Постраничная навигация
function donav($num, $table, $where = false, $page = false)
{
	if($page == false) {$page = 1;}
	if(($posts = docount($table,$where)) != false)
	{
		$navigation["count"] = $posts;
		$total = intval(($posts - 1) / $num) + 1; 
		$page = intval($page); 
		if(empty($page) or $page < 0) 
		{
			$page = 1; 
		}
		if($page > $total)
		{
			$page = $total;
		}
		$navigation["start"] = $page * $num - $num;
		$navigation["num"] = $num;
		for($i=1; $i<=$total; $i++)
		{
			$navigation["list"][] = $i;
		}
		return $navigation;
	}
	else
	{
		return false;
	}
}
?>