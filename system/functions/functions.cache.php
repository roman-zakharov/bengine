<?php

	#Проверка работоспособности memcashe и его подключение
	if(extension_loaded('memcache') and class_exists('Memcache') and MEMCACHE == true)
	{
		$memcache = new Memcache;
		$md5site = md5($cfg["url"]);
		if((@$memcache->pconnect('localhost', 11211)) == false) {
			error('Ошибка подключения memcache',__FILE__,__LINE__,__FUNCTION__);
			die('Ошибка подключения memcache');
		}
	} else {
		$memcache = false;
		$md5site = md5($cfg["url"]);
	}

	#Добавление данных в кэш
	function cacheAdd($pl, $pc = array())
	{
		global $memcache, $md5site;	
		
		$columns = 'id';
		
		$t = dodescribe($pl);
		
		foreach($t as $k => $v) {
			if($k != "id") {
				$columns .= ",`".$k."`";
			}
		}
		
		#Сортировка по возрастанию или по убыванию
		if(!empty($pc["order"]) and $pc["order"] == true) {
			$order = "DESC";
		} else {
			$order = "";
		}
		
		#По какому полю производить сортировку
		if(!empty($pc["sort"])) {
			$sort = $pc["sort"];
		} else {
			$sort = "id";
		}
		
		#Вытягиваем все данные таблицы
		$sql = doquery("SELECT ".$columns." FROM `".$pl."` ORDER BY `".$sort."` ".$order." LIMIT ".CACHE_LIMIT);
		
		if(dorows($sql) > 0) {
			$array = doarray($sql);
			if(isset($pc["keyid"]) and ($pc["keyid"] == true or $pc["keyid"] == 1))	{
				foreach($array as $v) {
					$cache[$v["id"]] = $v;
				}
			} else {
				$cache = $array;
			}
		} else {
			$cache = array();
		}
		
		if($memcache === false) {
			$cache_serialize = serialize($cache);
			if(file_put_contents(ROOT_DIR."/cache/cache_".$pl.".dat", $cache_serialize)) {
				return $cache;
			}
		} else {
			$pl = $pl."_".$md5site;
			if($memcache->set($pl, $cache, MEMCACHE_COMPRESSED, 2592000)) {
				return $cache;
			}
		}
		
		error('Не удалось создать кэш для плагина '.$pl,__FILE__,__LINE__,__FUNCTION__);
		return false;
	}

	#Получение данных из кэша
	function cacheGet($key)
	{
		global $memcache, $md5site;
		
		if($memcache === false) {
			$file_cache = ROOT_DIR."/cache/cache_".$key.".dat";
			if(file_exists($file_cache)) {
				$cache = file_get_contents($file_cache);
				$cache = unserialize($cache);
			} else {
				$cache = false;
			}
		} else {
			$key = $key."_".$md5site;
			$cache = $memcache->get($key);
		}
		if($cache != false) {
			return $cache;
		} else {
			return false;
		}
	}

	#Удаление данных из кэша
	function cacheDel()
	{
		global $memcache;
		
		if($memcache === false) {
			if(bengine_cleaner(ROOT_DIR."/cache")) {
				return true;
			}
		} else {
			if($memcache->flush()) {
				bengine_cleaner(ROOT_DIR."/cache");
				return true;
			}
		}
		error('Не удалось очистить кэш',__FILE__,__LINE__,__FUNCTION__);
		return false;
	}

	#Статистика данных кэша
	function cacheStat() {
		global $memcache;
		if($memcache === false) {
			return true;
		} else {
			mpr($memcache->getStats());
		}
	}

?>