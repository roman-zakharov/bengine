<?php


#Список файлов в папке
function bengine_files($dir, $path = false)
{
	$return = array();
	if(($opendir = @opendir($dir)) == false) {
		die("Ошибка чтения папки с шаблоном сайта. Поправьте конфигурацию");
	}
	while(($file = readdir($opendir)) !== false)
	{
		if(is_file($dir."/".$file) && $file != "." && $file != ".." && $file != "_thumbs") 
		{	
			if($path == false) {
				$return[] = $file;
			}
			else {
				$return[] = $dir.$file;
			}
		}
	}
	sort($return,SORT_STRING);
	return $return;
}

#Список папок в директории
function bengine_dirs($dir)
{
	if(is_dir($dir))
	{
		$opendir = opendir($dir);
		while(($folder = readdir($opendir)) !== false)
		{
			if(is_dir($dir."/".$folder) && $folder != "." && $folder != ".." && $folder != "_thumbs") {
				$return[] = $folder;
			}
		}
		if(isset($return) and count($return) > 0) {
			return $return;
		}
		else {
			return false;
		}
	}
	else {
		return false;
	}
}

#Размер папки
function bengine_dir_size($dir)
{
	$totalsize = 0;
	
	if($dirstream = opendir($dir))
	{
		while($filename = readdir($dirstream))
		{
			if (($filename != ".")&&($filename != "..")&&($filename != ".htaccess"))
			{
				if (is_file($dir."/".$filename)){$totalsize+=filesize($dir."/".$filename);}
				if (is_dir($dir."/".$filename)){$totalsize+=dir_size($dir."/".$filename);}
			}
		}
		closedir($dirstream);
	}
	return $totalsize;
}

#Очистка папки и удаление файлов
function bengine_cleaner($dir)
{
	if($dirstream = opendir($dir))
	{
		while($filename = readdir($dirstream))
		{
			if(($filename != ".")&&($filename != ".."))
			{
				if(is_file($dir."/".$filename))
				{
					if(!unlink($dir."/".$filename)) {
						return false;
					}
				}
				if(is_dir($dir."/".$filename))
				{
					bengine_cleaner($dir."/".$filename);
					if(!rmdir($dir."/".$filename)) {
						return false;
					}
				}
			}
		}
		closedir($dirstream);
		return true;
	}
	else {
		return false;
	}
}

#Создание папки
function bengine_new_dir($tempdir)
{
	if(!is_dir($tempdir))
	{
		if((mkdir($tempdir, 0777)) === false) {
			return false;
		}
		else {
			return true;
		}
	}
	else {
		return true;
	}
}


?>