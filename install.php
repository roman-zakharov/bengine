<?php

#Вывод ошибок
ini_set("display_errors", 0);
error_reporting(0);

#Заголовки страницы
header("Content-Type: text/html; charset=utf-8");

#Настраиваем по дефолту зону
date_default_timezone_set("Asia/Yekaterinburg");

#Старт сессии
session_start();

#константы
define("BENGINE", true);
define("ROOT_DIR", str_replace("\\","/",dirname(__FILE__)));
include_once(ROOT_DIR."/system/constants.php");
include_once(ROOT_DIR."/system/functions/functions.bengine.php");
include_once(ROOT_DIR."/system/functions/functions.files.php");

if(!is_writable(ROOT_DIR."/cache")) {
	@chmod(ROOT_DIR."/cache", 0777);
}
if(!is_writable(ROOT_DIR."/system")) {
	@chmod(ROOT_DIR."/system", 0777);
}
if(!is_writable(ROOT_DIR."/uploads")) {
	@chmod(ROOT_DIR."/uploads", 0777);
}

$echo = "";
$version = "3.2.5";

$sql[] = "DROP TABLE IF EXISTS `config`;";
$sql[] = "DROP TABLE IF EXISTS `pages`;";
$sql[] = "DROP TABLE IF EXISTS `plugins`;";
$sql[] = "DROP TABLE IF EXISTS `users`;";
$sql[] = "DROP TABLE IF EXISTS `users_fields`;";
$sql[] = "DROP TABLE IF EXISTS `groups`;";

$sql[] = "
CREATE TABLE IF NOT EXISTS `config` (
  `module` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  KEY `module` (`module`),
  KEY `type` (`type`),
  FULLTEXT KEY `value` (`value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
";

$sql[] = "
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `child` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Количество подстраниц',
  `parent` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Родительская страница',
  `showchild` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'Показывать подстраниц',
  `menu` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `order` int(10) unsigned NOT NULL DEFAULT '0',
  `engname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `plugin` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pages',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` longtext COLLATE utf8_unicode_ci NOT NULL,
  `header` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'header.tpl',
  `body` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'body.tpl',
  `footer` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'footer.tpl',
  PRIMARY KEY (`id`),
  UNIQUE KEY `engname` (`engname`),
  KEY `child` (`child`),
  KEY `parent` (`parent`),
  KEY `menu` (`menu`),
  KEY `order` (`order`),
  KEY `plugin` (`plugin`),
  KEY `image` (`image`),
  KEY `header` (`header`),
  KEY `body` (`body`),
  KEY `footer` (`footer`),
  FULLTEXT KEY `title` (`title`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `keywords` (`keywords`),
  FULLTEXT KEY `text` (`text`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
";

$sql[] = "
CREATE TABLE IF NOT EXISTS `plugins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `install` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `title` (`title`),
  KEY `admin` (`admin`),
  KEY `install` (`install`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci PACK_KEYS=0 AUTO_INCREMENT=1 ;
";

$sql[] = "
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `root` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `login` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `passw` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `menu` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `group` int(10) unsigned NOT NULL DEFAULT '1',
  `reg` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `auht` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`),
  UNIQUE KEY `email` (`email`),
  KEY `root` (`root`),
  KEY `passw` (`passw`),
  KEY `menu` (`menu`),
  KEY `group` (`group`),
  KEY `reg` (`reg`),
  KEY `auht` (`auht`),
  KEY `ip` (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
";

$sql[] = "
CREATE TABLE IF NOT EXISTS `users_fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order` int(10) unsigned NOT NULL DEFAULT '1',
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `minimal` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `check` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `order` (`order`),
  KEY `name` (`name`),
  KEY `title` (`title`),
  KEY `type` (`type`),
  KEY `minimal` (`minimal`),
  KEY `check` (`check`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
";

$sql[] = "
CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `permission` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`),
  KEY `admin` (`admin`),
  FULLTEXT KEY `permission` (`permission`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
";

if(isset($_POST["base"]) and $_POST["base"] != "") {
	$sql[] = "ALTER DATABASE `".$_POST["base"]."` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci";
}
	
$url = str_replace("http://","",$_SERVER["HTTP_HOST"]);
$url = str_replace("/","",$url);
$url = str_replace("www.","",$url);

#========================================================================================================================
#Установка CMS
if(isset($_POST) and count($_POST) > 0 and isset($_POST["admin_passw"]))
{
	#Проверка соединения с БД
	if(isset($_GET["setup"]) and $_GET["setup"] == "db")
	{
		$db = @mysqli_connect($_POST["host"],$_POST["user"],$_POST["pass"],$_POST["base"]);
		if(!$db or @mysqli_connect_errno()) {
			die("<span style=\"color:red\">Ошибка соединения с БД, проверьте настройки.</span>");
		}
		else {	
			sleep(1);		
			die("1");
		}
	}
	#создание файла конфигурации
	elseif(isset($_GET["setup"]) and $_GET["setup"] == "file")
	{
	
		
	$configDir = ROOT_DIR."/system";
	
	$file = "<?php\n
	\$host[\"host\"] = \"".$_POST["host"]."\";
	\$host[\"base\"] = \"".$_POST["base"]."\";
	\$host[\"user\"] = \"".$_POST["user"]."\";
	\$host[\"pass\"] = \"".$_POST["pass"]."\";\n
?>";

		if(!is_writable($configDir)) {
			die("<font color=\"red\">Ошибка прав доступа к папке ".$configDir.", измените вручную на 0777.</font>");
		}
		if(file_put_contents($configDir."/config.php",$file)) {
			sleep(1);
			die("1");
		} else {
			die("<span style=\"color:red\">Ошибка записи файла конфигурации.</span>");
		}	
	}
	
	#устанавливаем таблицы в БД
	elseif(isset($_GET["setup"]) and $_GET["setup"] == "table")
	{
		$sql[] = "INSERT INTO `users` (`id`, `root`, `login`, `passw`, `email`, `menu`, `group`, `reg`, `auht`, `ip`) VALUES (1, 1, '".$_POST["admin_login"]."', '".md5($_POST["admin_passw"])."', '".$_POST["admin_login"]."@".$url."', 1, 1, '".DATETIME."', '".DATETIME."', '".IP."');";
		$sql[] = "INSERT INTO `groups` (`id`, `title`, `admin`, `permission`) VALUES (1, 'Администраторы', 1, 'a:6:{s:5:\"pages\";s:2:\"on\";s:5:\"users\";s:2:\"on\";s:7:\"plugins\";s:2:\"on\";s:9:\"templates\";s:2:\"on\";s:6:\"config\";s:2:\"on\";s:6:\"manual\";s:2:\"on\";}');";
		$sql[] = "INSERT INTO `config` (`module`, `type`, `value`) VALUES
			('system', 'vers', '".$version."'),
			('system', 'url', '".$url."'),
			('system', 'email', 'admin@".$url."'),
			('system', 'title', 'Новый сайт'),
			('system', 'description', ''),
			('system', 'keywords', ''),
			('system', 'page', '0'),
			('system', 'errors', '0'),
			('system', 'header', 'header.tpl'),
			('system', 'body', 'body.tpl'),
			('system', 'footer', 'footer.tpl'),
			('system', 'error404', '404.tpl'),
			('system', 'offline', '0'),
			('system', 'offline_title', 'Сайт закрыт. Приносим свои извинения за доставленные неудобства.'),
			('system', 'notepad', '');
		";
		$db = mysqli_connect($_POST["host"],$_POST["user"],$_POST["pass"],$_POST["base"]);
		
		mysqli_query($db,"SET NAMES UTF8");
		foreach($sql as $v)
		{
			if(!mysqli_query($db,$v)) {
				die("<font color=\"red\">Ошибка установки таблицы. ".mysqli_error($db)."</font>");
			}
		}

		sleep(1);
		if(!unlink(ROOT_DIR."/install.php")) {
			rename(ROOT_DIR."/install.php",ROOT_DIR."/_install.php");
		}
		die("1");
	}
	else {
		die("<span style=\"color:red\">Install Error!</span>");
	}
}

#========================================================================================================================

$echo = "<html>
<head>
<title>Установка Bengine CMS</title>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<link rel='icon' href='system/template/favicon.ico' />
<link rel='stylesheet' href='system/template/css/style.css' type='text/css' media='screen' />
<script type='text/javascript' src='system/template/js/jquery.js'></script>
<script type='text/javascript'>
$(document).ready(function(){
	$('.body').show();
	$('input[type=text]').addClass('text');
	$('input[type=password]').addClass('text');
	$('input[type=submit],button').addClass('submit');	
	$('#submit').click(function(){
		if(document.getElementById('base').value == '') { alert('Заполните Название БД'); return false; }
		if(document.getElementById('user').value == '') { alert('Заполните Пользователь БД'); return false; }
		if(document.getElementById('admin_login').value == '') { alert('Заполните логин администратора'); return false; }
		if(document.getElementById('admin_passw').value == '') { alert('Заполните пароль администратора'); return false; }
		$('#result').html('Проверка подключения к БД...<br /><br />');
		$.post('/install.php?setup=db', $('#edit').serializeArray(), function(data){
			if(data == 1){
				$('#result').html('Записываем данные в файл конфигурации.<br /><br />');
				$.post('/install.php?setup=file', $('#edit').serializeArray(), function(data){
					if(data == 1){
						$('#result').html('Файл config.php успешно создан. Устанавливаем таблицы.<br /><br />');
						$.post('/install.php?setup=table', $('#edit').serializeArray(), function(data){
							if(data == 1){
								$('#result').html('Таблицы успешно установлены. <a href=\"/admin/\">Продолжить</a>.<br /><br />');
								$('#submit').hide();
							} else {
								$('#result').html(data+'<br /><br />');
							}
						});
					} else {
						$('#result').html(data+'<br /><br />');
					}
				});
			} else {
				$('#result').html(data+'<br /><br />');
			}
		});
		return false;
	});
});
</script>
</head>
<body>	
	<div class=\"body\" style=\"margin-top: 50px; width: 600px;\">
		<br /><br />
		<form id=\"edit\" method=\"post\">
			<fieldset>
				<legend>Настройка доступа к базе данных:</legend>
				<span id=\"result\" style=\"font: 12px Verdana; color: green;\"></span>
				<label for=\"host\">Сервер базы данных:</label> <input type=\"text\" id=\"host\" name=\"host\" value=\"localhost\"><br />
				<label for=\"user\">Пользователь БД:</label> <input type=\"text\" id=\"user\" name=\"user\" value=\"\"><br />
				<label for=\"pass\">Пароль пользователя:</label> <input type=\"password\" id=\"pass\" name=\"pass\" value=\"\"><br />
				<label for=\"base\">Название БД:</label> <input type=\"text\" id=\"base\" name=\"base\" value=\"\"><br />
				<label for=\"admin_login\">Логин администратора:</label>
				<input type=\"text\" id=\"admin_login\" name=\"admin_login\" value=\"\"><br />
				<label for=\"admin_passw\">Пароль администратора:</label>
				<input type=\"password\" id=\"admin_passw\" name=\"admin_passw\" value=\"\"><br />
				<label for=\"submit\"></label>
				<input type=\"submit\" id=\"submit\" name=\"setup\" value=\"Установить Bengine\"><br />
			</fieldset>
		</form>
	</div>
</body>
</html>";

echo $echo;
?>