<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>{$cfg.title}</title>
	<link href="/template/css/bootstrap.min.css" rel="stylesheet">
    <!--[if lt IE 9]><script src="https://oss.maxcdn.com/libs/html5shiv/r29/html5shiv.js"></script><![endif]-->
    <!--[if lt IE 9]><script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script><![endif]-->
	<script src="/template/js/jquery.min.js"></script>
	<script src="/template/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
	<nav class="navbar navbar-default" role="navigation">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Навигация</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">{$cfg.title}</a>
		</div>
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">			
				{if $cache_pages and count($cache_pages) > 0}		
					{foreach $cache_pages as $v}
						{if $v.parent == 0 and $v.menu == 1}
							<li {if isset($nodes[0]) and $nodes[0] == $v.engname}class="active"{/if}><a href="/{$v.engname}/">{$v.title}</a></li>
						{/if}
					{/foreach}
				{/if}
			</ul>
		</div>
	</nav>