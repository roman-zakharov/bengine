<?php
header("Content-Type: text/html; charset=utf-8");
mb_internal_encoding("UTF-8");
setlocale(LC_ALL,"ru_RU.UTF-8");

#Вывод ошибок
ini_set("display_errors", 1);

#Настраиваем по дефолту зону
date_default_timezone_set("Asia/Yekaterinburg");

#Старт сессии
session_start();

#константы
define("ROOT_DIR", str_replace("\\","/",dirname(__FILE__)));

#список констант
if(file_exists(ROOT_DIR."/system/constants.php")) {
	include_once(ROOT_DIR."/system/constants.php");
}

#установка системы
if(file_exists(ROOT_DIR."/install.php") and !file_exists(ROOT_DIR."/system/config.php")) {
	header("Location: /install.php"); die();
}

#Проверка прав доступа к кэшу или uploads
if(!is_writable(ROOT_DIR."/cache") or !is_writable(ROOT_DIR."/uploads")) {
    if(!mkdir(ROOT_DIR."/cache",0777) and !mkdir(ROOT_DIR."/uploads",0777)) {
        die("Измение права доступа для папок cache, uploads и файла /system/config.php");
    }
}

#основные рабочие массивы
$content = array();
$plugins = array();
$config  = array();
$nodes   = array();
$host 	 = array();
$page 	 = array();
$cfg 	 = array();
$nav 	 = array();
$pl 	 = "system";
$p 	 	 = 1;

#основные рабочие переменные
$result = "";

#Проверяем файл конфигурации
if(!file_exists(ROOT_DIR."/system/config.php")) {
	die("Отсутствует файл конфигурации config.php");
} else {
	include_once(ROOT_DIR."/system/config.php");
}

#Информация о конфигурации системы
include_once(ROOT_DIR."/system/functions/functions.mysqli.php");
$sql_cfg = doquery("SELECT * FROM config");
$cfg_array = doarray($sql_cfg);
foreach($cfg_array as $v) {
	if($v["module"] == "system") {
		$cfg[$v["type"]] = $v["value"];
	}
	$config[$v["module"]][$v["type"]] = $v["value"];
}

#Получение настроек доступа к БД
if(isset($cfg["errors"]) and $cfg["errors"] == "1") {
	error_reporting(E_ALL);
} else {
	error_reporting(0);
}

#Файлы ядра системы
include_once(ROOT_DIR."/system/functions/functions.bengine.php");
include_once(ROOT_DIR."/system/functions/functions.files.php");
include_once(ROOT_DIR."/system/functions/functions.cache.php");
include_once(ROOT_DIR."/system/functions/functions.seo.php");

#Обработка запроса строки браузера
$nodes = nodes();

#Постраничная навигация, узнаем текущую страницу
if(isset($_GET["p"]) and is_numeric($_GET["p"]) > 0) {
	$p = (int)$_GET["p"];
}

#Формирование кэша страниц
if(($cache_pages = cacheGet('pages')) === false) {
	$cache_pages = cacheAdd('pages', array(
		'sort' => 'order',
		'keyid' => 1,
	));
}
#Формирование кэша пользователей
if(($cache_users = cacheGet('users')) === false) {
	$cache_users = cacheAdd('users', array(
		'keyid' => 1
	));
}
	
#Работа с данными, администрирование или контент
if(isset($nodes[0]) and $nodes[0] == "admin")
{
	include_once(ROOT_DIR."/system/admin.php");
	include_once(ROOT_DIR . $header);
	include_once(ROOT_DIR . $body);
	include_once(ROOT_DIR . $footer);
}
else
{
	include_once(ROOT_DIR."/system/smarty/Smarty.class.php");
	$smarty = new Smarty();
	$smarty->setTemplateDir(ROOT_DIR.'/template');
	$smarty->setCompileDir(ROOT_DIR.'/cache/');
	$smarty->setConfigDir(ROOT_DIR.'/cache/');
	$smarty->setCacheDir(ROOT_DIR.'/cache/');
	
	include_once(ROOT_DIR."/system/engine.php");
	
	$smarty->assign('template','/template');
	$smarty->assign('cache_pages',$cache_pages);
	$smarty->assign('cache_users',$cache_users);
	$smarty->assign('content',$content);
	$smarty->assign('result',$result);
	$smarty->assign('config',$config);
	$smarty->assign('nodes',$nodes);
	$smarty->assign('page',$page);
	$smarty->assign('cfg',$cfg);
	$smarty->assign('nav',$nav);
	$smarty->assign('p',$p);
	
	$smarty->display(ROOT_DIR . $header);
	$smarty->display(ROOT_DIR . $body);
	$smarty->display(ROOT_DIR . $footer);
}

?>