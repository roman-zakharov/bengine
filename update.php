<?php

#Вывод ошибок
ini_set("display_errors", 0);
error_reporting(0);

#Заголовки страницы
header("Content-Type: text/html; charset=utf-8");

#Настраиваем по дефолту зону
date_default_timezone_set("Asia/Yekaterinburg");

#Старт сессии
session_start();

#константы
define("BENGINE", true);
define("ROOT_DIR", str_replace("\\","/",dirname(__FILE__)));

include_once(ROOT_DIR."/system/constants.php");
include_once(ROOT_DIR."/system/config.php");
include_once(ROOT_DIR."/system/functions/functions.mysqli.php");
include_once(ROOT_DIR."/system/functions/functions.bengine.php");
include_once(ROOT_DIR."/system/functions/functions.files.php");

if(!is_writable(ROOT_DIR."/cache")) {
	@chmod(ROOT_DIR."/cache", 0777);
}
if(!is_writable(ROOT_DIR."/system")) {
	@chmod(ROOT_DIR."/system", 0777);
}
if(!is_writable(ROOT_DIR."/uploads")) {
	@chmod(ROOT_DIR."/uploads", 0777);
}

$version = "3.2.5";

#========================================================================================================================

#Проверка соединения с БД
if(isset($_GET["setup"]) and $_GET["setup"] == "db")
{
	if(doquery("SELECT `value` FROM `config` WHERE `module`='system' and `type`='vers' LIMIT 1") == false) {
		die('<span style="color:red">Ошибка соединения с БД, проверьте настройки.</span>');
	}
	sleep(1);
	die("1");
}

#обновления
elseif(isset($_GET["setup"]) and $_GET["setup"] == "update")
{
	$error = 0;
	
	#Обновления
	$sql[] = "UPDATE `config` SET value='".$version."' WHERE `module`='system' and `type`='vers' LIMIT 1";
	
	foreach($sql as $v) {
		if(!doquery($v)) {
			error('Ошибка установки обновления: "'.$v.'"',__FILE__,__LINE__);
			$error++;
		}
	}
	
	sleep(3);
	
	if($error > 0) {
		die('<span style="color:red">Не все обновления были установлены, смотрите файл .error</span>');
	} else {	
		if(!unlink(ROOT_DIR."/update.php")) {
			die("2");
		} else {
			die("1");
		}
	}
}
	
#========================================================================================================================
?>

<html>
<head>
<title>Установка Bengine CMS</title>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<link rel='icon' href='system/template/favicon.ico' />
<link rel='stylesheet' href='system/template/css/style.css' type='text/css' media='screen' />
<script type='text/javascript' src='system/template/js/jquery.js'></script>
<script type='text/javascript'>
$(document).ready(function(){
	$('.body').show();
	$('#submit').click(function(){
		$('#result').html('Проверка подключения к БД...<br /><br />');
		$.get('/update.php?setup=db', function(data){
			if(data == 1){
				$('#result').html('Идет установка обновлений, ждите...<br /><br />');
				$.get('/update.php?setup=update', function(data){
					if(data > 0){
						var delupd = '';
						if(data == 2) {
							delupd = ' Удалите файл update.php!';
						}
						$('#result').html('Обновления успешно установлены.'+delupd+'<br />Перейдите в <a href="admin/">панель администрирования сайтом</a>.<br />');
						$('#submit').hide();
					}
					else {
						$('#result').html(data+'<br /><br />');
					}
				});
			} else {
				$('#result').html(data+'<br /><br />');
			}
		});
		return false;
	});
});
</script>
</head>
<body>
	<div class="body" style="margin-top: 50px; width: 600px;">
		<br /><br />
		<form id="edit" method="post">
			<fieldset>
				<legend>Обновление системы</legend>
				<span id="result" style="font: 12px Verdana; color: green;"></span>
				<input type="submit" id="submit" name="setup" value="Установить обновления до версии «<?php echo $version; ?>»">
			</fieldset>
		</form>
	</div>
</body>
</html>
